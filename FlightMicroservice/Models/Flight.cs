﻿namespace FlightMicroservice.Models
{
    public class Flight
    {
        internal static readonly int BUSINESS_SEATS_PER_ROW = 4;
        internal static readonly int ECONOMY_SEATS_PER_ROW = 6;

        public int Id { get; private set; }
        public int AirlineId { get; internal set; }
        public string Origin { get; internal set; }
        public string Destination { get; internal set; }
        public DateTime DepartureTime { get; internal set; }
        public DateTime ArrivalTime { get; internal set; }
        public int EconomyCapacity { get; internal set; }
        public int BusinessCapacity { get; internal set; }
        public decimal EconomyPrice { get; internal set; }
        public decimal BusinessPrice { get; internal set; }
        public ICollection<Seat> Seats { get; internal set; }

        public Flight() => Seats = new List<Seat>();

        public Flight(int airlineId,
                      string origin, 
                      string destination,  
                      DateTime departureTime, 
                      DateTime arrivalTime,
                      int economyCapacity, 
                      int businessCapacity, 
                      decimal economyPrice, 
                      decimal businessPrice
            )
        {
            if(!IsCapacityValid(economyCapacity, ECONOMY_SEATS_PER_ROW))
                throw new ArgumentException($"Capacity of Economy seats must be a multiple of {ECONOMY_SEATS_PER_ROW}");

            if (!IsCapacityValid(businessCapacity, BUSINESS_SEATS_PER_ROW))
                throw new ArgumentException($"Capacity of Business seats must be a multiple of {BUSINESS_SEATS_PER_ROW}");

            AirlineId = airlineId;
            Origin = origin;
            Destination = destination;
            DepartureTime = departureTime;
            ArrivalTime = arrivalTime;
            EconomyCapacity = economyCapacity;
            BusinessCapacity = businessCapacity;
            EconomyPrice = economyPrice;
            BusinessPrice = businessPrice;
            Seats = new List<Seat>();
        }

        private bool IsCapacityValid(int numberOfSeats, int multipleOf)
        {
            return numberOfSeats % multipleOf == 0;
        }
    }
}
