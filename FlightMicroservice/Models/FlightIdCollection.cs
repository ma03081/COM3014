﻿namespace FlightMicroservice.Models
{
    public class FlightIdCollection
    {
        public required List<int> FlightIds { get; set; }
    }
}
