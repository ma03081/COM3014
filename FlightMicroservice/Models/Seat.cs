﻿using System.Text.Json.Serialization;

namespace FlightMicroservice.Models
{
    public class Seat
    {
        public int Id { get; private set; }
        public int FlightId { get; internal set; }
        public string SeatNumber { get; internal set; }
        public ClassType ClassType { get; internal set; }
        public bool IsAvailable { get; internal set; }
        public Flight Flight { get; internal set; }

        public Seat() { }

        public Seat(int flightId, string seatNumber, ClassType classType, bool isAvailable)
        {
            FlightId = flightId;
            SeatNumber = seatNumber;
            ClassType = classType;
            IsAvailable = isAvailable;
        }
    }

    public enum ClassType
    {
        BUSINESS = 0,
        ECONOMY = 1
    }

}
