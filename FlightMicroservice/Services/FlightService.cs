﻿using FlightMicroservice.Models;
using Microsoft.EntityFrameworkCore;

namespace FlightMicroservice.Services
{
    public class FlightService : IFlightService
    {
        private readonly ApplicationDbContext dbContext;

        public FlightService(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Flight? GetFlight(int flightId)
        {
            Flight? flight = dbContext.Flights.SingleOrDefault(flight => flight.Id == flightId);
            return flight;
        }

        public List<Flight> GetFlights(int? airlineId, string? origin, string? destination, DateTime? departureTime, DateTime? arrivalTime)
        {
            IQueryable<Flight> query = dbContext.Flights.AsQueryable();

            if(airlineId != null)
                query = query.Where(flight => flight.AirlineId == airlineId);

            if (!string.IsNullOrWhiteSpace(origin))
                query = query.Where(flight => flight.Origin == origin);

            if (!string.IsNullOrWhiteSpace(destination))
                query = query.Where(flight => flight.Destination == destination);

            if (departureTime.HasValue)
                query = query.Where(flight => flight.DepartureTime.Date == departureTime.Value.Date);

            if (arrivalTime.HasValue)
                query = query.Where(flight => flight.ArrivalTime.Date == arrivalTime.Value.Date);

            return query.ToList();
        }

        public Flight CreateFlight(
            int airlineId,
            string origin, 
            string destination, 
            DateTime departureTime, 
            DateTime arrivalTime, 
            int economyCapacity, 
            int businessCapacity, 
            decimal economyPrice, 
            decimal businessPrice)
        {
            Flight flight = new Flight(airlineId, origin, destination, departureTime, arrivalTime, economyCapacity, businessCapacity, economyPrice, businessPrice);

            InitializeSeatsForFlight(flight);

            dbContext.Flights.Add(flight);
            dbContext.SaveChanges();

            return flight;
        }

        public bool RemoveFlight(int flightId) 
        { 
            Flight? flight = dbContext.Flights.SingleOrDefault(flight => flight.Id == flightId);
            if (flight == null)
                return false;

            dbContext.Flights.Remove(flight);
            dbContext.SaveChanges();

            return true;
        }

        public List<Seat> GetSeatsByFlightId(int flightId)
        {
            return dbContext.Seats
                .Where(seat => seat.FlightId == flightId) 
                .ToList();
        }

        public List<Flight> GetFlightsByIds(List<int> ids)
        {
            return dbContext.Flights
                            .Where(flight => ids.Contains(flight.Id))
                            .ToList();
        }


        private void InitializeSeatsForFlight(Flight flight)
        {
            int businessRows = flight.BusinessCapacity / Flight.BUSINESS_SEATS_PER_ROW;
            int economyRows = flight.EconomyCapacity / Flight.ECONOMY_SEATS_PER_ROW;

            // Initialize Business class seats
            for (int row = 1; row <= businessRows; row++)
            {
                for (char seat = 'A'; seat < 'A' + Flight.BUSINESS_SEATS_PER_ROW; seat++)
                {
                    flight.Seats.Add(new Seat
                    {
                        ClassType = ClassType.BUSINESS,
                        SeatNumber = $"{row}{seat}",
                        IsAvailable = true
                    });
                }
            }

            // Initialize Economy class seats
            int startingEconomyRow = businessRows + 1; // Economy rows start after the last Business row
            for (int row = startingEconomyRow; row < startingEconomyRow + economyRows; row++)
            {
                for (char seat = 'A'; seat < 'A' + Flight.ECONOMY_SEATS_PER_ROW; seat++)
                {
                    flight.Seats.Add(new Seat
                    {
                        ClassType = ClassType.ECONOMY,
                        SeatNumber = $"{row}{seat}",
                        IsAvailable = true
                    });
                }
            }
        }

    }


}

