﻿using FlightMicroservice.Models;

namespace FlightMicroservice.Services
{
    public interface IFlightService
    {
        Flight? GetFlight(int flightId);
        List<Flight> GetFlights(int? airlineId, string? origin, string? destination, DateTime? departureTime, DateTime? arrivalTime);
        Flight CreateFlight(int airlineId, string origin, string destination, DateTime departureTime, DateTime arrivalTime, int economyCapacity, int businessCapacity, decimal economyPrice, decimal businessPrice);
        bool RemoveFlight(int flightId);
        List<Seat> GetSeatsByFlightId(int flightId);
        List<Flight> GetFlightsByIds(List<int> ids);
    }
}
