﻿using FlightMicroservice.Models;

namespace FlightMicroservice.Services
{
    public interface ISeatService
    {
        List<Seat> GetSeats();
        Seat? GetSeat(int id);
        void BookSeat(int id);
        bool IsAvailable(int id);
    }
}
