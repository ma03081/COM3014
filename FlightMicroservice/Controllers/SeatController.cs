﻿using FlightMicroservice.Models;
using FlightMicroservice.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FlightMicroservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SeatController : ControllerBase
    {
        private readonly ISeatService seatService;

        public SeatController(ISeatService seatService)
        {
            this.seatService = seatService;        
        }

        [HttpGet]
        public ActionResult GetSeats() 
        {
            List<Seat> seats = seatService.GetSeats();
            if(seats == null)
                return BadRequest();

            return Ok(seats);
        }

        [HttpGet("{id}")]
        public ActionResult GetSeat(int id)
        {
            Seat? seat = seatService.GetSeat(id);
            if(seat == null)
                return NotFound($"Could Not Find Seat of Id: {id}");

            return Ok(seat);
        }

        [Authorize]
        [HttpPut("{id}")]
        public ActionResult BookSeat(int id)
        {
            try
            {
                seatService.BookSeat(id);
                return Ok();
            } 
            catch (KeyNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("{id}/isAvailable")]
        public ActionResult IsAvailable(int id)
        {
            try
            {
                bool isAvailable = seatService.IsAvailable(id);
                return Ok(isAvailable);
            }
            catch (KeyNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }            
        }
    }
}
