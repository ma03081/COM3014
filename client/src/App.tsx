import { Outlet } from 'react-router-dom';
import AuthProvider from './providers/AuthProvider';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import './App.scss';

function App() {
  return (
    <>
      <AuthProvider>
        <div className='wrapper'>
          <Header></Header>
          <div className='main'>
            <Outlet></Outlet>
          </div>
          <div className='footer-wrapper'>
            <Footer></Footer>
          </div>
        </div>
      </AuthProvider>
    </>
  );
}

export default App;
