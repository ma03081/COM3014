const airportMap = new Map<string, string>();
airportMap.set('London Heathrow', 'LHW');
airportMap.set('London Gatwick', 'LGW');
airportMap.set('Manchester', 'MAN');
airportMap.set('London Stanstead', 'STN');
airportMap.set('London Luton', 'LTN');
airportMap.set('Edinburgh', 'EDI');
airportMap.set('Birmingham', 'BHX');
airportMap.set('Bristol', 'BRS');
airportMap.set('Glasgow', 'GLA');
airportMap.set('Belfast International', 'BFS');
airportMap.set('Newcastle', 'NCL');
airportMap.set('Liverpool', 'LPL');
airportMap.set('Norwich', 'NWI');

export const airports = [...airportMap.keys()];

export function airportCode(airport: string): string {
  return airportMap.get(airport) ?? 'unknown';
};
