export function getSearchParam(requestURL: string, param: string): string {
  const url = new URL(requestURL);
  return url.searchParams.get(param) ?? '';
}
