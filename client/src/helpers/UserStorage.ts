class UserStorage {
    static storeUserId(id: string): void {
      localStorage.setItem('userId', id);
    }
  
    static getUserId(): string | null {
      return localStorage.getItem('userId');
    }
  }
  
  export default UserStorage;