export const formatLongDateTime = (dateTimeInput?: string) => {
  if (
    dateTimeInput === null ||
    typeof dateTimeInput === "undefined" ||
    dateTimeInput === ""
  ) {
    return "";
  } else {
    return new Date(dateTimeInput).toLocaleString("en-GB", {
      hour12: true,
      month: "short",
      year: "numeric",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
    }); //toLocaleString([], { hour12: true });
  }
};
