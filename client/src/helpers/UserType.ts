import { IUser } from '../providers/AuthProvider';

export function userStringToType(user: string) {
  return user.toLowerCase() === 'customer' ? 0 : 1;
}

export function userToDashboard(user?: IUser) {
  const isAirline = user?.type === 1;
  return isAirline ? 'airline-dashboard' : 'customer-dashboard';
}
