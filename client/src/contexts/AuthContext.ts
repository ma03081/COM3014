import { createContext } from 'react';
import { IUser } from '../providers/AuthProvider';

interface IAuthContext {
  isAuth: boolean;
  giveAuth: () => void;
  removeAuth: () => void;
  user?: IUser;
  updateUser: (newUser: IUser) => void;
}

export const AuthContext = createContext<IAuthContext>({
  isAuth: false,
  giveAuth: () => console.error('no give auth function'),
  removeAuth: () => console.error('no remove auth function'),
  updateUser: () => console.error('no update user function')
});
