import { ReactNode, useEffect, useState } from 'react';
import { useLoaderData } from 'react-router-dom';
import { AuthContext } from '../contexts/AuthContext';

export interface IUser {
  id: number;
  email: string;
  username: string;
  type: number;
}

function AuthProvider({ children }: { children: ReactNode }) {
  const [auth, setAuth] = useState(true);
  const [user, setUser] = useState<IUser>();
  const data = useLoaderData() as IUser | null;

  useEffect(() => {
    if (data) {
      giveAuth();
      setUser(data);
    } else {
      removeAuth();
      setUser(undefined);
    }
  }, []);

  const giveAuth = () => {
    setAuth(true);
  };

  const removeAuth = () => {
    setAuth(false);
  };

  const updateUser = (newUser: IUser) => {
    setUser(newUser);
  }

  return (
    <AuthContext.Provider value={{ isAuth: auth, giveAuth, removeAuth, user, updateUser }}>
      {children}
    </AuthContext.Provider>
  );
}

export default AuthProvider;
