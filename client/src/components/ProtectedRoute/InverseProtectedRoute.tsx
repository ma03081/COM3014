import { Navigate, Outlet } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';

function InverseProtectedRoute() {
  const { isAuth } = useAuth();

  if (!isAuth) {
    return <Outlet></Outlet>
  }

  return <Navigate to={'/'}></Navigate>
}

export default InverseProtectedRoute;
