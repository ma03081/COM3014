import { Outlet } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';
import ErrorPage from '../ErrorPage/ErrorPage';

function CustomerProtectedRoute() {
  const { user } = useAuth();

  if (user?.type === 0) {
    return <Outlet></Outlet>
  }

  return <ErrorPage></ErrorPage>
}

export default CustomerProtectedRoute;
