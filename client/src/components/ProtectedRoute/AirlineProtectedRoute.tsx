import { Outlet } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';
import ErrorPage from '../ErrorPage/ErrorPage';

function AirlineProtectedRoute() {
  const { user } = useAuth();

  if (user?.type === 1) {
    return <Outlet></Outlet>
  }

  return <ErrorPage></ErrorPage>
}

export default AirlineProtectedRoute;
