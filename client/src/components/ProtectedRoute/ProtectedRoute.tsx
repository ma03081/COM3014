import { Navigate, Outlet } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';

function ProtectedRoute() {
  const { isAuth } = useAuth();

  if (isAuth) {
    return <Outlet></Outlet>
  }

  return <Navigate to={'login'}></Navigate>
}

export default ProtectedRoute;
