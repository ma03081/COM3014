import { useLoaderData } from 'react-router-dom';
import { IFlight } from '../../services/Dashboard/CustomerDashboard';
import FlightCard from '../Dashboard/FlightCard/FlightCard';
import './FlightList.scss';

interface IFlightList {
  $values: IFlight[];
}

function FlightList() {
  const data = useLoaderData() as IFlightList | null;
  const flights = data?.$values ?? [];

  return (
    <>
      <div className='full-flight-list'>
        <div className='full-flight-list-card'>
          <span className='list-title'>Flight List</span>
          {flights.length > 0 ? (
            flights.map((flight) => {
              return (
                <FlightCard
                  key={flight.id}
                  flight={flight}
                  extraInfo={true}
                />
              );
            })
          ) : (
            <span className='list-subtitle'>
              No flights have been created yet.
            </span>
          )}
        </div>
      </div>
    </>
  );
}

export default FlightList;
