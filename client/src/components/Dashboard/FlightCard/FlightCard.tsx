import { Link } from 'react-router-dom';
import { IFlight } from '../../../services/Dashboard/CustomerDashboard';
import './FlightCard.scss';

interface IFlightCard {
  flight: IFlight;
  extraInfo?: boolean;
}

function FlightCard({ flight }: IFlightCard) {
  return (
    <>
    <div className='flight-card'>
      <span>Flight Number: {flight.id}</span>
      <span>Departure Time: {new Date(flight.departureTime).toLocaleString()}</span>
      <span>Arrival Time: {new Date(flight.arrivalTime).toLocaleString()}</span>
      <span className='flight-path'>{flight.origin} - {flight.destination}</span>
      <Link to={'/Flight/' + flight.id}>View Flight Info</Link>
    </div>
    </>
  );
}

export default FlightCard;

