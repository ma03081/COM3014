import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { AxiosError } from 'axios';
import { useAuth } from '../../../hooks/useAuth';
import avatar from './avatar.jpg';
import plane from './airplane.jpg';
import { patchUser } from '../../../services/PatchUser/PatchUser';
import './Profile.scss';

export interface IProfileForm {
  name: string;
  email: string;
  password: string;
  confirmPassword: string;
}

function Profile() {
  const { user, updateUser } = useAuth();
  const [disabled, setDisabled] = useState(true);
  const [error, setError] = useState('');
  const isAirline = user?.type === 1;
  const defaultValues: IProfileForm = {
    email: user?.email ?? '',
    name: user?.username ?? '',
    password: '',
    confirmPassword: ''
  };
  const { register, handleSubmit, getValues, setValue } = useForm<IProfileForm>({ mode: 'onChange', defaultValues });

  const resetFormValues = () => {
    if (!getValues().name)
      setValue('name', user?.username ?? '');
  
    if (!getValues().email)
      setValue('email', user?.email ?? '');
  
    if (!getValues().password)
      setValue('password', '');
  
    if (!getValues().confirmPassword)
      setValue('confirmPassword', '');
  };

  resetFormValues();

  const toggleEdit = () => {
    setDisabled(!disabled);
    resetFormValues();
  };

  const onSubmit = async (formValue: IProfileForm) => {
    if (formValue.password.length < 7) {
      setError('password length must be greater than 7 characters');
      return;
    }

    if (formValue.password !== formValue.confirmPassword) {
      setError('password and confirm password must match');
      return;
    }

    setError('');
    
    try {
      if (user) {
        await patchUser(formValue, user.id);

        updateUser({
          id: user.id,
          username: formValue.name,
          email: formValue.email,
          type: user.type
        });
        toggleEdit();

      } else {
        setError('No user id, please relog');
      }
    } catch (error) {
      const errorMessage = (error as AxiosError).response?.data;

      if (typeof errorMessage == 'string') {
        setError(errorMessage);
      } else {
        setError('An unexpected error has occurred');
      }
    }
  };

  return (
    <>
      <div className='profile'>
        <div className='card bio-card'>
          <div className='flex'>
            <img src={isAirline ? plane : avatar} alt='avatar' className='avatar'></img>
            <span>{user?.username ?? ''}</span>
            <span>Loyal {isAirline ? 'Airline' : 'Customer'}</span>
          </div>
        </div>

        <div className='card'>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className='form-h'>
              <div className='form-group-h'>
                <label>Full Name</label>
                <input type='text' {...register('name', { required: true, disabled })} />
              </div>

              <div className='form-group-h'>
                <label>Email</label>
                <input type='email' {...register('email', { required: true, disabled })} />
              </div>

              <div className='form-group-h'>
                <label>Password</label>
                <input type='password' placeholder='Enter new password' {...register('password', { required: true, disabled })} />
              </div>

              <div className='form-group-h'>
                <label>Confirm Password</label>
                <input type='password' placeholder='Confirm new password' {...register('confirmPassword', { required: true, disabled })} />
              </div>

              <div className='form-group-h'>
                <button type='button' onClick={toggleEdit}>Toggle Edit</button>
                <button type='submit' disabled={disabled}>Submit</button>
              </div>

              <div className='form-group-h'>
                {error && <span>{error}</span>}
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default Profile;
