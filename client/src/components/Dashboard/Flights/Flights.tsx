import { useLoaderData } from "react-router";
import { ICustomerDashboardData } from "../../../services/Dashboard/CustomerDashboard";
import { IAirlineDashboardData } from "../../../services/Dashboard/AirlineDashboard";
import FlightCard from "../FlightCard/FlightCard";
import "./Flights.scss";
import { Link, useNavigate } from "react-router-dom";

function Flights() {
  const data = useLoaderData() as
    | ICustomerDashboardData
    | IAirlineDashboardData;
  const navigate = useNavigate();

  const onClick = (flightType: string) => {
    navigate(`/customer/bookings?flightType=${flightType}`);
  };
  if (data.type === "customer") {
    return (
      <>
        <div className="flights">
          <div className="flex-row">
            <span className="flights-title">Upcoming Flights</span>
            <button
              type="button"
              className="view-more"
              onClick={() => onClick("upcoming")}
            >
              View more
            </button>
          </div>
          <div className="flight-list">
            {data.upcomingFlights.length > 0 ? (
              data.upcomingFlights.slice(0, 3).map((item) => {
                return <FlightCard key={item.flight.id} flight={item.flight}></FlightCard>;
              })
            ) : (
              <div>No Upcoming Flights</div>
            )}
          </div>
        </div>

        <div className="flights">
          <div className="flex-row">
            <span className="flights-title">Flights History</span>
            <button
              type="submit"
              className="view-more"
              onClick={() => onClick("previous")}
            >
              View more
            </button>
          </div>
          <div className="flight-list">
            {data.flightsHistory.length > 0 ? (
              data.flightsHistory.slice(0, 3).map((item) => {
                return <FlightCard key={item.flight.id} flight={item.flight}></FlightCard>;
              })
            ) : (
              <div>No Flights History</div>
            )}
          </div>
        </div>
      </>
    );
  } else {
    return (
      <>
        <div className="flights">
          <div className="flex-row">
            <span className="flights-title">Flights List</span>
            <Link to="/register-flight">
              <button type="button" className="view-more">
                Add flight
              </button>
            </Link>
          </div>
          <div className="flight-list">
            {data.flightList.length > 0 ? (
              data.flightList.slice(0, 3).map((flight) => {
                return (
                  <FlightCard key={flight.id} flight={flight}></FlightCard>
                );
              })
            ) : (
              <div>No Flights have been created yet.</div>
            )}
          </div>
        </div>
      </>
    );
  }
}

export default Flights;
