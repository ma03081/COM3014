import Profile from './Profile/Profile';
import Flights from './Flights/Flights';
import './Dashboard.scss';

function Dashboard() {
  return (
    <>
      <div className='dashboard'>
        <Profile></Profile>
        <Flights></Flights>
      </div>
    </>
  );
}

export default Dashboard;
