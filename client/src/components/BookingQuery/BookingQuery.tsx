import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { airports } from '../../helpers/Airports';
import './BookingQuery.scss';

interface IBookingQuery {
  origin: string;
  destination: string;
  date: string;
  seatType: string;
}

function BookingQuery() {
  const navigate = useNavigate();
  const [error, setError] = useState('');
  const { register, handleSubmit } = useForm<IBookingQuery>({mode: 'onChange', defaultValues: { origin: '', destination: '', seatType: ''}});

  const onSubmit = (query: IBookingQuery) => {
    if (query.origin === query.destination) {
      setError('Destination cannot be the same as origin');
      return;
    }

    setError('');
    navigate(`/booking/list?origin=${query.origin}&destination=${query.destination}&date=${query.date}&seatType=${query.seatType}`);
  };

  return (
    <>
      <div className='booking-query'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='card booking-query-card'>
            <div className='form-group'>
              <label>Origin:</label>
              <select {...register('origin', { required: true })}>
                <option value={''} disabled>Select an airport</option>
                {airports.map((airport) => {
                  return <option key={airport} value={airport}>{airport}</option>
                })}
              </select>
            </div>

            <div className='form-group'>
              <label>Destination:</label>
              <select {...register('destination', { required: true })}>
                <option value='' disabled>Select an airport</option>
                {airports.map((airport) => {
                  return <option key={airport} value={airport}>{airport}</option>
                })}
              </select>
            </div>

            <div className='form-group'>
              <label>Departure Date:</label>
              <input type='date' min={new Date().toISOString().split('T')[0]} {...register('date', { required: true })}></input>
            </div>

            <div className='form-group'>
              <label>Seat Type:</label>
              <select {...register('seatType', { required: true })}>
                <option value='' disabled>Select Seat Type</option>
                <option value='economy'>Economy</option>
                <option value='business'>Business</option>
              </select>
            </div>

            <div className='form-group'>
              <button type='submit'>Submit</button>
            </div>

            <div className='form-group'>
              {error && <span>{error}</span>}
            </div>
          </div>
        </form>
      </div>
    </>
  );
}

export default BookingQuery;
