import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { AxiosError } from 'axios';
import { useAuth } from '../../hooks/useAuth';
import { loginUser } from '../../services/Login/Login';
import { userToDashboard } from '../../helpers/UserType';
import UserStorage from "../../helpers/UserStorage";
import './Login.scss';

export interface ILoginForm {
  email: string;
  password: string;
}

export function Login() {
  const { giveAuth, updateUser } = useAuth();
  const navigate = useNavigate();
  const [error, setError] = useState('');
  const { register, handleSubmit } = useForm<ILoginForm>({mode: 'onChange'});

  const onSubmit = async (formValue: ILoginForm) => {
    setError('');

    try {
      const result = await loginUser(formValue);
      if (result) {
        giveAuth();
        const userId = result.data.id.toString();
        UserStorage.storeUserId(userId)
        updateUser(result.data);
        navigate(`/${userToDashboard(result.data)}`);
      } else {
        setError('Incorrect credentials');
      }
    } catch (error) {
      const errorMessage = (error as AxiosError).response?.data;

      if (typeof errorMessage == 'string') {
        setError(errorMessage);
      } else {
        setError('An unexpected error has occurred');
      }
    }
  };

  return (
    <>
      <div className='login'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='card login-card'>
            <div className='form-group'>
              <label>Email Address</label>
              <input type='email' placeholder='Enter email' {...register('email', { required: true })} />
            </div>

            <div className='form-group'>
              <label>Password</label>
              <input type='password' placeholder='Enter password' {...register('password', { required: true })} />
            </div>

            <div className='form-group'>
              <button type='submit'>Submit</button>
            </div>

            <div className='form-group'>
              {error && <span>{error}</span>}
            </div>
          </div>
        </form>
      </div>
    </>
  );  
}

export default Login;
