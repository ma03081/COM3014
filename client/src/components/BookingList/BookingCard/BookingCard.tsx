import { useLocation, useNavigate } from "react-router-dom";
import { IFlight } from "../../../services/Dashboard/CustomerDashboard";
import "./BookingCard.scss";
import {
  BookingOrder,
} from "../../../services/BookingList/BookingOrder";

interface IBookingCard {
  flight: IFlight;
}

function BookingCard({ flight }: IBookingCard) {
  const location = useLocation();
  const navigate = useNavigate();
  const isEconomy = location.search.split("seatType=")[1] === "economy";

  const handleClick = async (event: any) => {
    event.preventDefault();
    //show loader
    const bookingInfo: BookingOrder = {
      flightId: flight.id,
      bookingClass: isEconomy ? 1 : 2,
      price: isEconomy ? flight.economyPrice : flight.businessPrice
    };
    try {
      alert("Flight booked successfully, navigating to payments page...");
      navigate('/booking/payment-form', { state: { bookingInfo } });
    } catch (error) {
      console.error("Booking failed:", error);
      alert("Failed to book flight. Please try again.  Error: ${error.message}");
    }
  };

  return (
    <>
      <div className="booking-card">
        <div className="booking-card-details">
          <div className="item">
            <span>Departure Time:</span>
            <span>{flight.departureTime}</span>
          </div>

          <div className="item">
            <span>Arrival Time:</span>
            <span>{flight.arrivalTime}</span>
          </div>
        </div>

        <div className="price">
          <div className="price-label">
            <span>{isEconomy ? "Economy" : "Business"} Price:</span>
            <span>
              £{isEconomy ? flight.economyPrice : flight.businessPrice}
            </span>
          </div>
          <button type="button" onClick={handleClick}>
            Purchase
          </button>
        </div>
      </div>
    </>
  );
}

export default BookingCard;
