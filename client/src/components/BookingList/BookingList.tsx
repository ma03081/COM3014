import { useLoaderData, useLocation } from "react-router-dom";
import { IBookingList } from "../../services/BookingList/BookingList";
import BookingCard from "./BookingCard/BookingCard";
import "./BookingList.scss";

function BookingList() {
  const data = useLoaderData() as IBookingList;
  const location = useLocation();
  const isEconomy = location.search.split("seatType=")[1] === "economy";
  const origin = decodeURIComponent(
    location.search.split("origin=")[1].split("&")[0]
  );
  const destination = decodeURIComponent(
    location.search.split("destination=")[1].split("&")[0]
  );
  const date = location.search.split("date=")[1].split("&")[0];

  return (
    <>
      <div className='booking-list'>
        <div className='card booking-list-card'>
          <div className='booking-list-title'>
            <span>Showing </span>
            <span className='booking-bold'>
              {isEconomy ? "Economy" : "Business"}
            </span>
            <span> Flights for </span>
            <span className='booking-bold'>
              {origin} - {destination}
            </span>
            <span> on </span>
            <span className='booking-bold'>
              {new Date(date).toLocaleDateString()}
            </span>
          </div>

          {data.flights.length > 0 ? (
            data.flights.map((flight) => {
              return (
                <BookingCard
                  key={flight.id}
                  flight={flight}
                ></BookingCard>
              );
            })
          ) : (
            <span className='booking-list-subtitle'>No Flights Available for Selected Parameters.</span>
          )}
        </div>
      </div>
    </>
  );
}

export default BookingList;
