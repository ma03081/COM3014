import './Home.scss';

function Home() {
  return (
    <>
      <div className='home'>
        <div className='card home-card'>
          <p>Welcome to Airline Booking!</p>
          <p>This website allows you to create and book domestic flights in the UK</p>
          <p>If you are new please navigate to the register page via the link in the header</p>
          <p>Happy flying</p>
        </div>
      </div>
    </>
  );
}

export default Home;
