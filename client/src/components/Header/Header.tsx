import { NavLink } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';
import { userToDashboard } from '../../helpers/UserType';
import './Header.scss';

type NavLinkRenderProps = { isActive: boolean };

function Header() {
  const { isAuth, user } = useAuth();
  
  const activeClass = (props: NavLinkRenderProps) => {
    if (props.isActive)
      return 'nav-item-active';
    return 'nav-item';
  };

  return (
    <>
      <div className='header'>
        <div className='header-content'>
          <span className='header-title'>Airline Booking</span>

          <nav className='nav'>
            <NavLink to={'/'} className={activeClass}>Home</NavLink>

            {isAuth ?
            <div>
              <NavLink to={userToDashboard(user)} className={activeClass} >Dashboard</NavLink>
              {user?.type === 0 && <NavLink to={'customer/bookings'} className={activeClass}>Bookings List</NavLink>}
              {user?.type === 0 && <NavLink to={'booking/query'} className={activeClass}>Book a Flight</NavLink>}
              {user?.type === 1 && <NavLink to={`flights?id=${user.id}`} className={activeClass}>Flight List</NavLink>}
              <NavLink to={'logout'} className={activeClass}>Logout</NavLink>
            </div> :
            <div>
              <NavLink to={'login'} className={activeClass}>Login</NavLink>
              <NavLink to={'register'} className={activeClass}>Register</NavLink>
            </div>}
          </nav>
        </div>
      </div>
    </>
  );
}

export default Header;
