import { useLoaderData } from "react-router-dom";
import "./CustomerBookings.scss";
import { IBookingInfo } from "../../services/BookingView/BookingView";
import CustomerBookingCard from "./CustomerBookingCard";

const CustomerBookings = () => {
  const bookings = useLoaderData() as IBookingInfo[];
  let flightType = location.search.split("flightType=")[1]?.split("&")[0];
  if (typeof flightType === "undefined") {
    flightType = "upcoming";
  }

  return (
    <div className="page-view">
      <div className="customer-booking-view">
        <h1 className="headings">List of bookings</h1>
        {bookings.length > 0 ?(bookings.map((booking) => (
          <CustomerBookingCard
            flightId={booking.flightId}
            flightType={flightType}
            bookingId={booking.id}
            key={booking.id}
          />
        ))):(
          <p className="messages">No bookings have been made yet.</p>
        ) }
      </div>
    </div>
  );
};

export default CustomerBookings;
