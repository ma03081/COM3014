import { useEffect, useState } from "react";
import { formatLongDateTime } from "../../helpers/DateTimeHelpers";
import { GetBookingFlight } from "../../services/BookingView/BookingView";
import { IFlight } from "../../services/Dashboard/CustomerDashboard";
import { useNavigate } from "react-router-dom";

const CustomerBookingCard = ({
  flightId,
  flightType,
  bookingId,
}: {
  flightId: number;
  flightType: string;
  bookingId: number;
}) => {
  const [flightData, setFlightData] = useState<IFlight | undefined>();
  const navigate = useNavigate();

  const onClick = () => {
    navigate(`/booking/${bookingId}`);
  };
  useEffect(() => {
    GetBookingFlight(flightId).then((data) => {
      setFlightData(data);
    });
  }, [flightId]);
  return (
    <div>
      {flightData ? (
        <div>
          {new Date(flightData.departureTime).getTime() >=
            new Date().getTime() &&
            flightType === "upcoming" && (
              <div className="card customer-booking-view-card">
                <div>Origin: {flightData?.origin}</div>
                <div>Destination: {flightData?.destination}</div>
                <div>
                  Depature: {formatLongDateTime(flightData?.departureTime)}
                </div>
                <button type="submit" className="view-more" onClick={onClick}>
                  View details
                </button>
              </div>
            )}

          {new Date(flightData.departureTime).getTime() <
            new Date().getTime() &&
            flightType === "previous" && (
              <div className="card customer-booking-view-card">
                <div>Origin: {flightData?.origin}</div>
                <div>Destination: {flightData?.destination}</div>
                <div>
                  Depature: {formatLongDateTime(flightData?.departureTime)}
                </div>
                <button type="submit" className="view-more" onClick={onClick}>
                  View details
                </button>
              </div>
            )}
        </div>
      ) : (
        ""
      )}
    </div>
  );
};
export default CustomerBookingCard;
