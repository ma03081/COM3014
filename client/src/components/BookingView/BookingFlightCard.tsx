import { useEffect, useState } from "react";
import { GetBookingFlight } from "../../services/BookingView/BookingView";
import "./BookingView.scss";
import { IFlight } from "../../services/Dashboard/CustomerDashboard";
import { formatLongDateTime } from "../../helpers/DateTimeHelpers";

const BookingFlightCard = ({ flightId }: { flightId: number }) => {
  const [flightData, setFlightData] = useState<IFlight | undefined>();

  useEffect(() => {
    GetBookingFlight(flightId).then((data) => {
      setFlightData(data);
    });
  }, [flightId]);

  return (
    <div className="card booking-view-card">
      <h3>Flight Information</h3>
      <div>Origin: {flightData?.origin}</div>
      <div>Destination: {flightData?.destination}</div>
      <div>Depature: {formatLongDateTime(flightData?.departureTime)}</div>
      <strong>Arrival: {formatLongDateTime(flightData?.arrivalTime)}</strong>
    </div>
  );
};

export default BookingFlightCard;
