import { useLoaderData } from "react-router-dom";
import BookingFlightCard from "./BookingFlightCard";
import BookingSeatCard from "./BookingSeatCard";
import "./BookingView.scss";
import { IFullBookingInfo } from "../../services/BookingView/BookingView";

const BookingView = () => {
  const flightData = useLoaderData() as IFullBookingInfo;

  return (
    <div className="booking-view">
      {flightData && <BookingFlightCard flightId={flightData.booking.flightId} />}
      {flightData && <BookingSeatCard seatId={flightData.booking.seatId} />}
    </div>
  );
};

export default BookingView;
