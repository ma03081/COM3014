import { useEffect, useState } from "react";
import { GetBookingSeat, IFullBookingInfo, updateSeat } from "../../services/BookingView/BookingView";
import { ISeat } from "../../services/Dashboard/CustomerDashboard";
import { useLoaderData } from "react-router-dom";
import { useForm } from "react-hook-form";
import { AxiosError } from "axios";

interface ISeatForm {
  seat: string;
}

const BookingSeatCard = ({ seatId }: { seatId?: number }) => {
  const data = useLoaderData() as IFullBookingInfo;
  const { register, watch } = useForm<ISeatForm>({ mode: 'onChange', defaultValues: { seat: '' } });
  const [seatData, setSeatData] = useState<ISeat | undefined>();

  useEffect(() => {
    const { unsubscribe } = watch(async (form) => {
      if (form.seat) {
        try {
          const result = await updateSeat(data.booking.id, +form.seat);

          if (result.data.bookedSeat) {
            GetBookingSeat(+form.seat).then((data) => {
              setSeatData(data);
              alert('succesfully booked seat');
            });
          } else {
            alert('Unable to book seat')
          }

        } catch (error) {
          const errorMessage = (error as AxiosError).response?.data;

          if (typeof errorMessage == 'string') {
            alert(errorMessage);
          } else {
            alert('An unexpected error has occurred');
          }
        }
      }
    });

    return () => unsubscribe()
  }, [watch]);


  useEffect(() => {
    if (!seatId) {
      return;
    }
    GetBookingSeat(seatId).then((data) => {
      setSeatData(data);
    });
  }, [seatId]);

  const bookingClassType = data.booking.bookingClass === 2 ? 0 : 1
  const seats = data.seats.$values.filter((seat) => seat.classType === bookingClassType && seat.isAvailable);

  return (
    <div className="card booking-view-card">
      <h3>Seat Information</h3>
      
      <form>
        <label>Seat:</label>
        <select className="seat-selector" {...register('seat', { required: true })} disabled={!!seatData}>
          {
            !seatData ?
            <option value='' disabled>Select a Seat:</option> :
            <option value={seatData.id}>{seatData.seatNumber}</option>
          }
          {
            seats.map((seat) => {
              return <option key={seat.id} value={seat.id}>{seat.seatNumber}</option>
            })
          }
        </select>
      </form>
    </div>
  );
};

export default BookingSeatCard;
