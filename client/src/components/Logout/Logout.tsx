import { useEffect } from 'react';
import { useLoaderData } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';
import './Logout.scss';

function Logout() {
  const { removeAuth } = useAuth();
  const error = useLoaderData();

  useEffect(() => {
    removeAuth();
  }, []);

  return (
    <>
      <div className='logout'>
        <div className='card logout-card'>
          {
            error ?
            <div className='logout-content'>
              <span>Logout failed</span>
              <span>Use the Header to navigate!</span>
            </div> :
            <div className='logout-content'>
              <span>Sucessfully logged out!</span>
              <span>Use the Header to navigate!</span>
            </div>
          }
        </div>
      </div>
    </>
  );
}

export default Logout;
