import { useLoaderData } from 'react-router-dom';
import { IFlightData } from '../../services/Flight/Flight';
import { ISeat } from '../../services/Dashboard/CustomerDashboard';
import './Flight.scss';

function Flight() {
  const data = useLoaderData() as IFlightData;
  const flight = data.flight;

  const calculateRows = (seats: ISeat[]) => {
    let curr = 0;
    let counter = 0;
    seats.forEach((seat) => {
      const num = +seat.seatNumber.match(/^(\d+)([A-Z]+)$/)![1];
      if (curr !== num) {
        counter++;
        curr = num;
      }
    });
    return counter;
  };

  const businessSeats = data.seats.$values.filter((seat) => seat.classType === 0);
  const businessRows = calculateRows(businessSeats);
  const economySeats = data.seats.$values.filter((seat) => seat.classType === 1);
  const economyRows = calculateRows(economySeats);

  return (
    <>
      <div className='flight'>
        <div className='card flight-data-card'>
          <span className='flight-data-title'>Flight Details</span>

          <div className='flight-data-item'>
            <span className='flight-data-label'>ID: </span>
            <span className='flight-data-value'>{flight.id}</span>
          </div>

          <div className='flight-data-item'>
            <span className='flight-data-label'>Origin: </span>
            <span className='flight-data-value'>{flight.origin}</span>
          </div>

          <div className='flight-data-item'>
            <span className='flight-data-label'>Destination: </span>
            <span className='flight-data-value'>{flight.destination}</span>
          </div>

          <div className='flight-data-item'>
            <span className='flight-data-label'>Departure Time: </span>
            <span className='flight-data-value'>{new Date(flight.departureTime).toLocaleString()}</span>
          </div>

          <div className='flight-data-item'>
            <span className='flight-data-label'>Arrival Time: </span>
            <span className='flight-data-value'>{new Date(flight.arrivalTime).toLocaleString()}</span>
          </div>

          <div className='flight-data-item'>
            <span className='flight-data-label'>Economy Capacity: </span>
            <span className='flight-data-value'>{flight.economyCapacity}</span>
          </div>

          <div className='flight-data-item'>
            <span className='flight-data-label'>Business Capacity: </span>
            <span className='flight-data-value'>{flight.businessCapacity}</span>
          </div>

          <div className='flight-data-item'>
            <span className='flight-data-label'>Economy Price: </span>
            <span className='flight-data-value'>{flight.economyPrice}</span>
          </div>

          <div className='flight-data-item'>
            <span className='flight-data-label'>Business Price: </span>
            <span className='flight-data-value'>{flight.businessPrice}</span>
          </div>

          <div className='flight-data-item seat-lists'>
            <div className='flight-data-item'>
              <span className='flight-data-label'>Business Seats:</span>
              <table className='flight-data-value'>
                <tbody>
                  {[...Array(businessRows)].map((_, rowIndex) => (
                    <tr key={rowIndex}>
                      {[...Array(4)].map((_, cellIndex) => {
                        const seatNumber = `${rowIndex + 1}${String.fromCharCode(65 + cellIndex)}`;
                        const seat = businessSeats.find(seat => seat.seatNumber === seatNumber);
                        const isAvailable = seat ? seat.isAvailable : false;
                        return (
                          <td key={cellIndex} className={isAvailable ? 'seat-available' : 'seat-unavailable'}>
                            {seatNumber}
                          </td>
                        );
                      })}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>

            <div className='flight-data-item'>
              <span className='flight-data-label'>Economy Seats:</span>
              <table className='flight-data-value'>
                <tbody>
                  {[...Array(economyRows)].map((_, rowIndex) => (
                    <tr key={rowIndex}>
                      {[...Array(6)].map((_, cellIndex) => {
                        const seatNumber = `${rowIndex + businessRows + 1}${String.fromCharCode(65 + cellIndex)}`;
                        const seat = economySeats.find(seat => seat.seatNumber === seatNumber);
                        const isAvailable = seat ? seat.isAvailable : false;
                        return (
                          <td key={cellIndex} className={isAvailable ? 'seat-available' : 'seat-unavailable'}>
                            {seatNumber}
                          </td>
                        );
                      })}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Flight;
