import './ErrorPage.scss';

function ErrorPage() {
  return (
    <>
      <div className='error-page'>
        <div className='card error-page-card'>
          Invalid Operation
        </div>
      </div>
    </>
  )
}

export default ErrorPage;
