import './ErrorPage.scss';

function ErrorPage404() {
  return (
    <>
      <div className='error-page'>
        <div className='card error-page-card'>
          404 page not found
        </div>
      </div>
    </>
  )
}

export default ErrorPage404;
