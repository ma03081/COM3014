import './Footer.scss';

function Footer() {
  return (
    <>
      <div className='footer'>
        <div className='footer-content'>
          <span>COM3014 Group 5</span>
        </div>
      </div>
    </>
  );
}

export default Footer;