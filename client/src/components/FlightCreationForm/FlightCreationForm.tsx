import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { AxiosError } from 'axios';
import { addFlight } from '../../services/FlightForm/FlightForm';
import { airports } from '../../helpers/Airports';
import './FlightCreationForm.scss';

export interface IFlightCreationForm {
  origin: string;
  destination: string;
  departure: string;
  arrival: string;
  economyCapacity: number;
  businessCapacity: number;
  economyPrice: number;
  businessPrice: number;
}

function FlightCreationForm() {
  const navigate = useNavigate();
  const [error, setError] = useState('');
  const [disabled, setDisabled] = useState(false);
  const { register, handleSubmit } = useForm<IFlightCreationForm>({ mode: 'onChange', defaultValues: { origin: '', destination: '' } });

  const onSubmit = async (formValue: IFlightCreationForm) => {
    if (!Number.isInteger(formValue.businessCapacity) || !Number.isInteger(formValue.economyCapacity)) {
      setError('Please enter an integer for the capacity.')
      return;
    }

    if (formValue.economyCapacity % 6 !== 0) {
      setError('Economy capacity must be a multiple of 6');
      return;
    }

    if (formValue.businessCapacity % 4 !== 0) {
      setError('Business capacity must be a multiple of 4');
      return;
    }

    if (formValue.origin === formValue.destination) {
      setError('Destination cannot be the same as the origin');
      return;
    }

    if (new Date(formValue.departure).getTime() > new Date(formValue.arrival).getTime()) {
      setError('Arrival time must be after departure time');
      return;
    }

    const today = new Date();
    today.setHours(0, 0, 0, 0); // Reset time to midnight for an accurate date-only comparison
    const departureDate = new Date(formValue.departure);
    departureDate.setHours(0, 0, 0, 0); // Ensure time components do not affect comparison

    if (departureDate <= today) {
      setError('Departure date must be at least for tomorrow.');
      return;
    }

    setError('');
    setDisabled(true);

    try {
      const result = await addFlight(formValue);
      navigate(`/flight/${result.data.id}`);
    } catch (error) {
      const errorMessage = (error as AxiosError).response?.data;

      if (typeof errorMessage == 'string') {
        setError(errorMessage);
      } else {
        setError('An unexpected error has occurred');
      }

      setDisabled(false);
    }
  }

  return (
    <>
      <div className='flightCreationForm'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='card flight-form-card'>
            <div className='inner-flight-form'>
              <div className='form-col'>
                <div className='form-group'>
                  <label>Origin</label>
                  <select {...register('origin', { required: true })} >
                    <option value={''} disabled>Select an airport</option>
                    {airports.map((airport) => {
                      return <option key={airport} value={airport}>{airport}</option>
                    })}
                  </select>
                </div>

                <div className='form-group'>
                  <label>Destination</label>
                  <select {...register('destination', { required: true })} >
                    <option value={''} disabled>Select an airport</option>
                    {airports.map((airport) => {
                      return <option key={airport} value={airport}>{airport}</option>
                    })}
                  </select>
                </div>

                <div className='form-group'>
                  <label>Departure Time</label>
                  <input type='datetime-local' placeholder='Enter departure time' {...register('departure', { required: true })} />
                </div>

                <div className='form-group'>
                  <label>Arrival Time</label>
                  <input type='datetime-local' placeholder='Enter arrival time' {...register('arrival', { required: true })} />
                </div>
              </div>

              <div className='form-col'>
                <div className='form-group'>
                  <label>Economy Class Capacity</label>
                  <input type='number' placeholder='Enter capacity' {...register('economyCapacity', { required: true, valueAsNumber: true })} />
                </div>

                <div className='form-group'>
                  <label>Business Class Capacity</label>
                  <input type='number' placeholder='Enter capacity' {...register('businessCapacity', { required: true, valueAsNumber: true })} />
                </div>

                <div className='form-group'>
                  <label>Economy Class Price</label>
                  <input type='number' placeholder='Enter price' {...register('economyPrice', { required: true, valueAsNumber: true })} />
                </div>

                <div className='form-group'>
                  <label>Business Class Price</label>
                  <input type='number' placeholder='Enter price' {...register('businessPrice', { required: true, valueAsNumber: true })} />
                </div>
              </div>
            </div>

            <div className='form-group'>
              <button type='submit' disabled={disabled}>Submit</button>
            </div>

            <div className='form-group'>
              {error && <span>{error}</span>}
            </div>
          </div>
        </form>
      </div>
    </>
  )
}

export default FlightCreationForm;
