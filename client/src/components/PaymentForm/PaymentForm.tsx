import { useState, useEffect } from 'react';
import { useLocation, useNavigate } from "react-router-dom";
import Api from "../../helpers/Api";
import { CardElement, useStripe, useElements, Elements } from '@stripe/react-stripe-js';
import { StripeCardElement, loadStripe } from '@stripe/stripe-js';
import {bookFlight} from "../../services/BookingList/BookingOrder";
import './PaymentForm.scss';

const stripePromise = loadStripe('pk_test_51P5UhOExQclpActcEGkHdut1X1k6uQnEXQ2cKKTD5l9FS9a1TyB2ap1lRSQXZt35Dpd7mh8gHOwFyVb4TiqpZfJr00bDRrD4vF');

const PaymentForm = () => {
    const stripe = useStripe();
    const elements = useElements();
    const location = useLocation();
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [bookingInfo, setBookingInfo] = useState<any>(null);
    const [message, setMessage] = useState("");
    const [clientSecret, setClientSecret] = useState("");

    useEffect(() => {
        if (location.state && location.state.bookingInfo) {
            setBookingInfo(location.state.bookingInfo);
        }
    }, [location]);

    useEffect(() => {
        if (!stripe || !bookingInfo) return;

        const fetchClientSecret = async () => {
            try {
                const form = { items: [{ price: bookingInfo.price * 100 }] }; 
                const response = await Api.post("Booking/create-payment-intent", form, { withCredentials: true });
                setClientSecret(response.data.clientSecret);
            } catch (error) {
                console.error('Failed to retrieve client secret:', error);
                setMessage("Failed to retrieve Stripe secret key");
            }
        };

        fetchClientSecret();
    }, [stripe, bookingInfo]);

    const handleSubmit = async (event: any) => {
        event.preventDefault();
        if (!stripe || !elements || !clientSecret) {
            console.log('Stripe.js has not yet loaded or client secret not available.');
            return;
        }
    
        setLoading(true);
        try {
            const { error, paymentIntent } = await stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                    card: elements.getElement(CardElement) as StripeCardElement,
                },
            });
    
            if (error) {
                setMessage(`Payment failed: ${error.message}`);
            } else if (paymentIntent && paymentIntent.status === 'succeeded') {
                setMessage("Payment succeeded!");
    
                const updatedBookingInfo = {
                    ...bookingInfo,
                    paymentIntentId: paymentIntent.id 
                };
                
                setBookingInfo(updatedBookingInfo);
                
                await bookFlight(updatedBookingInfo);
                alert("Flight booked successfully!");
                navigate('/customer/bookings');

            } else {
                setMessage(`Payment processed but check status: ${paymentIntent.status}`);
            }
        } catch (error) {
            console.error('Payment confirmation failed:', error);
            setMessage(`An error occurred while confirming the payment.`);
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="form-container">
            <form onSubmit={handleSubmit} className="payment-form">
                <h3 id="form-heading">Last step!</h3>
                <p id="form-description">Enter your payment info below.</p>
                <CardElement className="StripeElement" onChange={event => {
                    setMessage(event.error ? event.error.message : "");
                }} />
                <button type="submit" className="button" disabled={!stripe || loading}>
                    {loading ? 'Processing...' : 'Pay'}
                </button>
                {message && <div className="message">{message}</div>}
            </form>
        </div>
    );
};

export default () => (
    <Elements stripe={stripePromise}>
        <PaymentForm />
    </Elements>
);
