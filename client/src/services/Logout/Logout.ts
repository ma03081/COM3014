import { AxiosError } from 'axios';
import Api from '../../helpers/Api';

export async function LogoutUser(): Promise<AxiosError | null> {
  try {
    await Api.post('User/logout', {}, { withCredentials: true });
    return null;
  } catch (error) {
    return error as AxiosError;
  }
}
