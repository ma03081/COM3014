import Api from "../../helpers/Api";
import UserStorage from "../../helpers/UserStorage";
import { IFlight, ISeat } from "../Dashboard/CustomerDashboard";

export interface IBookingInfo {
  id: number;
  flightId: number;
  userId: number;
  bookingClass: number;
  seatId?: number;
}

export interface IBookingData {
  flight: IFlight;
  seat: ISeat;
}

export async function GetAllBookings(): Promise<IBookingInfo[]> {
  const url = `Booking`;
  try {
    const id = UserStorage.getUserId();
    const booking = await Api.get(`${url}?userId=${id}`, { withCredentials: true });

    return booking.data as IBookingInfo[];
  } catch (error) {
    throw error;
  }
}

