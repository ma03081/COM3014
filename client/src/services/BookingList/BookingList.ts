import Api from "../../helpers/Api";
import { getSearchParam } from "../../helpers/SearchParams";
import { IFlight } from "../Dashboard/CustomerDashboard";

export interface IBookingList {
  flights: IFlight[];
}

export async function GetBookingList({
  request,
}: {
  request: Request;
}): Promise<IBookingList> {
  const origin = getSearchParam(request.url, "origin");
  const destination = getSearchParam(request.url, "destination");
  const date = getSearchParam(request.url, "date");
  const seatType = getSearchParam(request.url, "seatType");

  const fullUrl = `Flight?origin=${origin}&destination=${destination}&departureDate=${date}&seatType=${seatType}`;

  try {
    const flight = await Api.get(`${fullUrl}`, { withCredentials: true });

    return {
      flights: flight.data.$values,
    };
  } catch (error) {
    throw error;
  }
}
