import { AxiosResponse } from "axios";
import Api from "../../helpers/Api";
import { IFlight } from "../Dashboard/CustomerDashboard";

export interface BookingOrder {
  flightId: number;
  bookingClass: number;
  seatId?: number;
  price: number;
}

export async function bookFlight(
  form: BookingOrder
): Promise<AxiosResponse<IFlight>> {
  return Api.post("Booking", form, { withCredentials: true });
}
