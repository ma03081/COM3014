import Api from "../../helpers/Api";
import { IBookingInfo } from "../BookingView/BookingView";

export interface ISeat {
  id: number;
  classType: number;
  seatNumber: string;
  isAvailable: boolean;
}

export interface ISeats {
  $id: string;
  $values: ISeat[];
}

export interface IFlight {
  id: number;
  origin: string;
  destination: string;
  arrivalTime: string;
  departureTime: string;
  economyCapacity: number;
  businessCapacity: number;
  economyPrice: number;
  businessPrice: number;
  seats: ISeats
}

export interface IFLightBookingData{
  flight: IFlight;
  booking: IBookingInfo;
}

export interface ICustomerDashboardData {
  type: 'customer'
  upcomingFlights: IFLightBookingData[];
  flightsHistory: IFLightBookingData[];
}

export async function GetCustomerDashboardData(): Promise<ICustomerDashboardData> {
  try {
    const upcomingFlights = Api.get('Booking/upcoming', { withCredentials: true });
    const flightsHistory = Api.get('Booking/history', { withCredentials: true });
    const [uData, hData] = await Promise.all([upcomingFlights, flightsHistory]);
    return {
      type: 'customer',
      upcomingFlights: uData.data,
      flightsHistory: hData.data
    };
  } catch (error) {
    try {
      const upcomingFlights = Api.get('Booking/upcoming', { withCredentials: true });
      const flightsHistory = Api.get('Booking/history', { withCredentials: true });
      const [uData, hData] = await Promise.all([upcomingFlights, flightsHistory]);
      return {
        type: 'customer',
        upcomingFlights: uData.data,
        flightsHistory: hData.data
      };
    } catch (newError) {
      throw newError;
    }
  }
}
