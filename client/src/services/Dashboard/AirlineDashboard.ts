import Api from "../../helpers/Api";
import UserStorage from "../../helpers/UserStorage";
import { IFlight } from "./CustomerDashboard";

export interface IAirlineDashboardData {
  type: "airline";
  flightList: IFlight[];
}

export async function GetAirlineDashboardData(): Promise<IAirlineDashboardData> {
  try {
    const id = UserStorage.getUserId();
    const response = await Api.get(`Flight?airlineId=${id}`, {
      withCredentials: true,
    });
    const flights = response.data.$values;
    return {
      type: "airline",
      flightList: flights,
    };
  } catch (error) {
    throw error;
  }
}
