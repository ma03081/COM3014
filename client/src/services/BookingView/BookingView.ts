import { Params } from "react-router-dom";
import Api from "../../helpers/Api";
import { IFlight, ISeat, ISeats } from "../Dashboard/CustomerDashboard";

export interface IBookingInfo {
  id: number;
  flightId: number;
  userId: number;
  bookingClass: number;
  seatId?: number;
}

export interface IFullBookingInfo {
  booking: IBookingInfo;
  seats: ISeats;
}

interface ISeatBookingResponse {
  bookedSeat: boolean;
  message: string;
}

export interface IBookingData {
  flight: IFlight;
  seat: ISeat;
}

export async function GetBookingInformation({
  params,
}: {
  params: Params;
}): Promise<IFullBookingInfo> {
  const url = `Booking/${params.id}`;

  const bookingResponse = await Api.get(url, { withCredentials: true });
  const flightId = bookingResponse.data.flightId;
  
  const seatsResponse = await Api.get(`Flight/${flightId}/seats`, { withCredentials: true });
  
  return {
    booking: bookingResponse.data,
    seats: seatsResponse.data
  };
  
}

export async function GetBookingFlight(
  flightId?: number
): Promise<IFlight | undefined> {
  if (!flightId) {
    return undefined;
  }
  try {
    const flights = await Api.get(`Flight/${flightId}`, {
      withCredentials: true,
    });

    return flights.data as IFlight;
  } catch (error) {
    throw error;
  }
}

export async function GetBookingSeat(seatId: number): Promise<ISeat> {
  try {
    const seat = await Api.get(`Seat/${seatId}`, {
      withCredentials: true,
    });

    return seat.data as ISeat;
  } catch (error) {
    throw error;
  }
}

export async function GetFullBookingData(
  flightId: number,
  seatId: number
): Promise<IBookingData> {
  try {
    console.log(seatId);
    const flight = Api.get(`Flight/${flightId}`, { withCredentials: true });
    const seats = Api.get(`Flight/${flightId}/seats`, {
      withCredentials: true,
    });
    const [fdata, sdata] = await Promise.all([flight, seats]);
    return {
      flight: fdata.data,
      seat: sdata.data,
    };
  } catch (error) {
    throw error;
  }
}

export async function updateSeat(booking: number, seatId: number) {
  return Api.put<ISeatBookingResponse>(`Booking/${booking}`, { SeatId: seatId }, { withCredentials: true });
}
