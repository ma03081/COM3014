import { AxiosResponse } from 'axios';
import Api from '../../helpers/Api';
import { IFlightCreationForm } from '../../components/FlightCreationForm/FlightCreationForm';
import { IFlight } from '../Dashboard/CustomerDashboard';

export async function addFlight(form: IFlightCreationForm): Promise<AxiosResponse<IFlight>> {
  return Api.post('Flight', {
    Origin: form.origin,
    Destination: form.destination,
    DepartureTime: form.departure,
    ArrivalTime: form.arrival,
    EconomyCapacity: form.economyCapacity,
    BusinessCapacity: form.businessCapacity,
    EconomyPrice: form.economyCapacity,
    BusinessPrice: form.businessPrice
  }, { withCredentials: true });
}
