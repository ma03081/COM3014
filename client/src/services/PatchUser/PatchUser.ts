import Api from '../../helpers/Api';
import { IProfileForm } from '../../components/Dashboard/Profile/Profile';

export function patchUser(form: IProfileForm, id: number) {
  return Api.patch(`User/${id}`, {
    Username: form.name,
    Email: form.email,
    Password: form.password
  }, { withCredentials: true });
}
