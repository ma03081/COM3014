import { AxiosResponse } from 'axios';
import Api from '../../helpers/Api';
import { ILoginForm } from '../../components/Login/Login';
import { IUser } from '../../providers/AuthProvider';

export async function loginUser(form: ILoginForm): Promise<AxiosResponse<IUser>> {
  return Api.post('User/login', {
    Email: form.email,
    Password: form.password
  }, { withCredentials: true });
}
