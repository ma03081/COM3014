import Api from '../../helpers/Api';
import { getSearchParam } from '../../helpers/SearchParams';

export async function GetFlightList({ request }: { request: Request}) {
  try {
    const id = getSearchParam(request.url, 'id');
    const result = await Api.get(`Flight?airlineId=${id}`, { withCredentials: true });
    return result.data;
  } catch (error) {
    return null;
  }
}
