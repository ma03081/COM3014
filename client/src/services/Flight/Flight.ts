import { Params } from 'react-router-dom';
import Api from '../../helpers/Api';
import { IFlight, ISeats } from '../Dashboard/CustomerDashboard';

export interface IFlightData {
  flight: IFlight;
  seats: ISeats;
}

export async function GetFlightData({ params }: { params: Params }): Promise<IFlightData> {
  try {
    const flight = Api.get(`Flight/${params.id}`, { withCredentials: true });
    const seats = Api.get(`Flight/${params.id}/seats`, { withCredentials: true });
    const [fdata, sdata] = await Promise.all([flight, seats]);
    return {
      flight: fdata.data,
      seats: sdata.data
    };
  } catch (error ){
    throw error;
  }    
}
