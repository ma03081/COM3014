import { AxiosResponse } from 'axios';
import Api from '../../helpers/Api';
import { IRegisterForm } from '../../components/Register/Register';
import { userStringToType } from '../../helpers/UserType';
import { IUser } from '../../providers/AuthProvider';

export async function registerUser(form: IRegisterForm): Promise<AxiosResponse<IUser>> {
  return Api.post('User/register', {
    Email: form.email,
    Username: form.name,
    Password: form.password,
    UserType: userStringToType(form.customerType)
  }, { withCredentials: true });
}
