import { AxiosResponse } from 'axios';
import Api from '../../helpers/Api';
import { IUser } from '../../providers/AuthProvider';

export async function AuthoriseUser(): Promise<AxiosResponse<IUser> | null> {
  try {
    const result = await Api.post('User/authorize', {}, { withCredentials: true });
    return result.data;
  } catch (error) {
    return null;
  }
}
