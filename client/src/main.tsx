import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import App from "./App.tsx";
import Login from "./components/Login/Login.tsx";
import Register from "./components/Register/Register.tsx";
import Logout from "./components/Logout/Logout.tsx";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute.tsx";
import InverseProtectedRoute from "./components/ProtectedRoute/InverseProtectedRoute.tsx";
import Dashboard from "./components/Dashboard/Dashboard.tsx";
import Flight from "./components/Flight/Flight.tsx";
import FlightList from "./components/FlightList/FlightList.tsx";
import BookingQuery from "./components/BookingQuery/BookingQuery.tsx";
import BookingList from "./components/BookingList/BookingList.tsx";
import PaymentFormWrapper   from "./components/PaymentForm/PaymentForm.tsx";
import { AuthoriseUser } from "./services/Authorise/Authorise.ts";
import { LogoutUser } from "./services/Logout/Logout.ts";
import { GetCustomerDashboardData } from "./services/Dashboard/CustomerDashboard.ts";
import { GetAirlineDashboardData } from "./services/Dashboard/AirlineDashboard.ts";
import { GetFlightData } from "./services/Flight/Flight.ts";
import { GetFlightList } from "./services/FlightList/FlightList.ts";
import { GetBookingList } from "./services/BookingList/BookingList.ts";
import "./index.scss";
import FlightCreationForm from "./components/FlightCreationForm/FlightCreationForm.tsx";
import { GetBookingInformation } from "./services/BookingView/BookingView.ts";
import { GetAllBookings } from "./services/CustomerBookings/CustomerBookings.ts";
import CustomerBookings from "./components/CustomerBookings/CustomerBookings.tsx";
import BookingView from "./components/BookingView/BookingView.tsx";
import CustomerProtectedRoute from "./components/ProtectedRoute/CustomerProtectedRoute.tsx";
import AirlineProtectedRoute from "./components/ProtectedRoute/AirlineProtectedRoute.tsx";
import ErrorPage404 from "./components/ErrorPage/ErrorPage404.tsx";
import ErrorPage from "./components/ErrorPage/ErrorPage.tsx";
import Home from "./components/Home/Home.tsx";
import { loadStripe } from '@stripe/stripe-js';

loadStripe('pk_test_51P5UhOExQclpActcEGkHdut1X1k6uQnEXQ2cKKTD5l9FS9a1TyB2ap1lRSQXZt35Dpd7mh8gHOwFyVb4TiqpZfJr00bDRrD4vF');

const router = createBrowserRouter([
  {
    path: "/",
    loader: AuthoriseUser,
    element: <App></App>,
    errorElement: <ErrorPage></ErrorPage>,
    children: [
      {
        path: "",
        element: <Home></Home>
      },
      {
        element: <InverseProtectedRoute></InverseProtectedRoute>,
        children: [
          {
            path: "login",
            element: <Login></Login>,
          },
          {
            path: "register",
            element: <Register></Register>,
          },
        ],
      },
      {
        element: <ProtectedRoute></ProtectedRoute>,
        children: [
          {
            path: "logout",
            loader: LogoutUser,
            element: <Logout></Logout>,
          },
          {
            element: <CustomerProtectedRoute></CustomerProtectedRoute>,
            children: [
              {
                path: "customer-dashboard",
                loader: GetCustomerDashboardData,
                element: <Dashboard></Dashboard>,
              },
              {
                path: "booking/query",
                element: <BookingQuery></BookingQuery>,
              },
              {
                path: "booking/list",
                loader: GetBookingList,
                element: <BookingList></BookingList>,
              },
              {
                path: "booking/payment-form",
                element: <PaymentFormWrapper></PaymentFormWrapper >,
              },
              {
                path: "customer/bookings",
                loader: GetAllBookings,
                element: <CustomerBookings></CustomerBookings>,
              },
              {
                path: "booking/:id",
                loader: GetBookingInformation,
                element: <BookingView></BookingView>,
              }
            ]
          },
          {
            element: <AirlineProtectedRoute></AirlineProtectedRoute>,
            children: [
              {
                path: "airline-dashboard",
                loader: GetAirlineDashboardData,
                element: <Dashboard></Dashboard>,
              },
              {
                path: "register-flight",
                element: <FlightCreationForm></FlightCreationForm>,
              }
            ]
          },
          {
            path: "flight/:id",
            loader: GetFlightData,
            element: <Flight></Flight>,
          },
          {
            path: "flights",
            loader: GetFlightList,
            element: <FlightList></FlightList>,
          },
        ],
      },
      {
        path: "error",
        element: <ErrorPage></ErrorPage>
      },
      {
        path: "*",
        element: <ErrorPage404></ErrorPage404>
      }
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <RouterProvider router={router}></RouterProvider>
  </React.StrictMode>
);
