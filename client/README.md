# Airline Booking System client

## How to develop the Client

Make sure you have docker (latest) and node installed (e.g. node version v20.11.0 which was used to create this).

1. Install vite globally via `npm install -g vite`
2. Comment out the client from the docker-compose file:
```yml
  # client:
  #   build:
  #     context: ./client
  #   image: client:${IMAGE_TAG}
  #   ports:
  #     - "${CLIENT_PORT}:4200"
```
3. Run `docker compose up -d --build`
4. Navigate to the client directory
5. Run `npm install`
6. Run `npm run dev`
7. Go to http://localhost:4200/ to view the app
8. Make changes save the files and refresh the page