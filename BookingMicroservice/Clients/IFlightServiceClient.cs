﻿using BookingMicroservice.Models;

namespace BookingMicroservice.Clients
{
    public interface IFlightServiceClient
    {
        Task<HttpResponseMessage> GetFlightCapacityAsync(int flightId, int classType);
        Task<HttpResponseMessage> IsSeatAvailableAsync(int seatId);
        Task<HttpResponseMessage> BookSeatAsync(int seatId);
        Task<HttpResponseMessage> GetFlightsByIdAsync(FlightIdCollection flightIdCollection);
    }
}
