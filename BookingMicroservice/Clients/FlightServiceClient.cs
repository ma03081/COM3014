﻿using BookingMicroservice.Models;

namespace BookingMicroservice.Clients
{
    public class FlightServiceClient : IFlightServiceClient
    {
        private readonly HttpClient httpClient;
        private const string FLIGHT_API_PATH = "api/Flight";
        private const string SEAT_API_PATH = "api/Seat";

        public FlightServiceClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }


        public async Task<HttpResponseMessage> GetFlightCapacityAsync(int flightId, int classType)
        {
            return await httpClient.GetAsync($"{FLIGHT_API_PATH}/{flightId}/capacity?classType={classType}");
        }

        public async Task<HttpResponseMessage> IsSeatAvailableAsync(int seatId)
        {
            return await httpClient.GetAsync($"{SEAT_API_PATH}/{seatId}/isAvailable");
        }

        public async Task<HttpResponseMessage> BookSeatAsync(int seatId)
        {
            return await httpClient.PutAsync($"{SEAT_API_PATH}/{seatId}", null);
        }

        public async Task<HttpResponseMessage> GetFlightsByIdAsync(FlightIdCollection flightIdCollection)
        {
            return await httpClient.PostAsJsonAsync($"{FLIGHT_API_PATH}/byIds", flightIdCollection);
        }

    }
}
