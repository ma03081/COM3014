using BookingMicroservice.Clients;
using BookingMicroservice.Handlers;
using BookingMicroservice.Models;
using BookingMicroservice.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpContextAccessor();
builder.Services.AddTransient<RequestCookieHandler>();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddLogging();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseMySql(builder.Configuration.GetConnectionString("DefaultConnection"),
    new MariaDbServerVersion(new Version(10, 4, 20))));

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"] ?? throw new InvalidOperationException("JWT Key is not configured."))),
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidIssuer = builder.Configuration["Jwt:Issuer"] ?? throw new InvalidOperationException("JWT Issuer is not configured."),
            ValidAudience = builder.Configuration["Jwt:Audience"] ?? throw new InvalidOperationException("JWT Audience is not configured.")
        };

        options.Events = new JwtBearerEvents
        {
            OnAuthenticationFailed = context =>
            {
                Console.WriteLine("Authentication failed: " + context.Exception.Message);
                return Task.CompletedTask;
            },
            OnTokenValidated = context =>
            {
                Console.WriteLine("Token validated");
                return Task.CompletedTask;
            }
        };
    });

builder.Services.AddHttpClient<IFlightServiceClient, FlightServiceClient>(client =>
{
    string? baseURL = builder.Configuration["FlightMicroservice:BaseUrl"];
    if (string.IsNullOrEmpty(baseURL))
        throw new InvalidOperationException("FlightMicroservice BaseUrl is not configured.");

    baseURL = baseURL.EndsWith("/") ? baseURL : baseURL + "/";
    client.BaseAddress = new Uri(baseURL);
}).AddHttpMessageHandler<RequestCookieHandler>(); ;

builder.Services.AddScoped<IBookingService, BookingService>();
builder.Services.AddScoped<IReservationComplianceService, ReservationComplianceService>();
builder.Services.AddScoped<IStripeService, StripeService>();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionHandler>();
app.UseHttpsRedirection();

// Middleware to check for the access token in cookies
app.Use(async (context, next) =>
{
    var accessToken = context.Request.Cookies["AccessToken"];
    if (!string.IsNullOrEmpty(accessToken))
    {
        context.Request.Headers.Append("Authorization", "Bearer " + accessToken);
    }

    await next();
});

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
app.Run();
