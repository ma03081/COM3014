using BookingMicroservice.Models;
using Stripe;

namespace BookingMicroservice.Services
{
    public interface IStripeService
    {
        Task<bool> VerifyPaymentIntent(string paymentIntentId);
        int CalculateOrderAmount(List<Item> items);
    }
}