﻿using BookingMicroservice.Models;

namespace BookingMicroservice.Services
{
    public interface IReservationComplianceService
    {
        Task<Booking?> TryCreateBookingAsync(int flightId, int userId, BookingClass bookingClass, int? seatId);
        Task TryBookSeatAsync(int bookingId, int seatId);
        Task<IEnumerable<FlightBookingInfo>> TryGetUpcomingFlightsAsync(int userId);
        Task<IEnumerable<FlightBookingInfo>> TryGetPreviousFlightsAsync(int userId);
    }
}
