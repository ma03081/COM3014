﻿using BookingMicroservice.Models;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace BookingMicroservice.Services
{
    public class BookingService : IBookingService
    {
        private readonly ApplicationDbContext dbContext;

        public BookingService(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public List<Booking> GetBookings(int? flightId = null, int? userId = null, BookingClass? bookingClass = null)
        {
            IQueryable<Booking> query = dbContext.Bookings.AsQueryable();

            if (flightId.HasValue)
                query = query.Where(booking => booking.FlightId == flightId.Value);

            if (userId.HasValue)
                query = query.Where(booking => booking.UserId == userId.Value);

            if (bookingClass.HasValue)
                query = query.Where(booking => booking.BookingClass == bookingClass.Value);

            List<Booking> bookings = query.ToList();
            return bookings;
        }

        public Booking CreateBooking(int flightId, int userId, BookingClass bookingClass)
        {
            Booking booking = new Booking(flightId, userId, bookingClass, null);

            dbContext.Bookings.Add(booking);
            dbContext.SaveChanges();

            return booking;
        }

        public Booking? GetBooking(int bookingId)
        {
            Booking? booking = dbContext.Bookings.SingleOrDefault(booking => booking.Id == bookingId);
            return booking;
        }

        public void DeleteBooking(int bookingId)
        {
            Booking? booking = GetBooking(bookingId);
            if (booking == null)
                throw new KeyNotFoundException($"A Booking with the provided Id: {bookingId} doesnt exist");

            dbContext.Bookings.Remove(booking);
            dbContext.SaveChanges();
        }

        public Booking UpdateBooking(int bookingId, int seatId)
        {
            Booking? booking = GetBooking(bookingId);
            if (booking == null)
                throw new KeyNotFoundException($"A Booking with the provided Id: {bookingId} doesnt exist");

            booking.SetSeatNumber(seatId);

            int affectedRows = dbContext.Bookings
            .Where(booking => booking.Id == bookingId)
            .ExecuteUpdate(setters => setters
                .SetProperty(booking => booking.SeatId, seatId));

            dbContext.SaveChanges();

            if (affectedRows == 0)
                throw new DbUpdateException("Operation was not able to update the Database");

            return booking;
        }
    }
}
