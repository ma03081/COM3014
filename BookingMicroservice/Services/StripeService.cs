using BookingMicroservice.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Stripe;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace BookingMicroservice.Services
{
    public class StripeService : IStripeService
    {
        private readonly ILogger<StripeService> _logger;
        private readonly IConfiguration _configuration;

        public StripeService(ILogger<StripeService> logger, IConfiguration configuration)
        {
            _logger = logger; 
            StripeConfiguration.ApiKey = configuration["Stripe:SecretKey"];
        }

        public async Task<bool> VerifyPaymentIntent(string paymentIntentId)
        {
            var service = new PaymentIntentService();
            try
            {
                var paymentIntent = await service.GetAsync(paymentIntentId);
                _logger.LogInformation($"Payment Intent Status: {paymentIntent.Status}");  
                return paymentIntent.Status == "succeeded";
            }
            catch (StripeException ex)
            {
                _logger.LogError($"Error verifying payment intent: {ex.Message}"); 
                throw new Exception("Failed to verify payment intent: " + ex.Message);
            }
        }

        public int CalculateOrderAmount(List<Item> items)
        {
            return items.Sum(item => item.Price);
        }
    }
}
