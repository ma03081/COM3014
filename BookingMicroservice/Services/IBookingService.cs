﻿using BookingMicroservice.Models;

namespace BookingMicroservice.Services
{
    public interface IBookingService
    {
        Booking CreateBooking(int flightId, int userId, BookingClass bookingClass);
        List<Booking> GetBookings(int? flightId = null, int? userId = null, BookingClass? bookingClass = null);
        Booking? GetBooking(int bookingId);
        void DeleteBooking(int bookingId);
        Booking UpdateBooking(int bookingId, int seatId);
    }
}
