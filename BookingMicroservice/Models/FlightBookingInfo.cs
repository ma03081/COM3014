﻿namespace BookingMicroservice.Models
{
    public class FlightBookingInfo
    {
        public Flight Flight { get; set; }
        public Booking Booking { get; set; }

        public FlightBookingInfo(Flight flight, Booking booking)
        {
            Flight = flight;
            Booking = booking;
        }
    }
}
