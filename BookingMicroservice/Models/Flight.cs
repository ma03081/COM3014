﻿namespace BookingMicroservice.Models
{
    public class Flight
    {
        public int Id { get; set; }
        public int AirlineId { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
    }
}
