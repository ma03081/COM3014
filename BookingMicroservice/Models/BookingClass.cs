﻿namespace BookingMicroservice.Models
{
    public enum BookingClass
    {
        BUSINESS = 0,
        ECONOMY = 1
    }
}
