namespace BookingMicroservice.Models
{
    public class PaymentIntentCreateRequest 
    {
        public List<Item> Items { get; set; }
    }

    public class Item
    {
        public int Price { get; set; }
    }

}