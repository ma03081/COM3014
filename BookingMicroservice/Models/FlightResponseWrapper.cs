﻿using System.Text.Json.Serialization;

namespace BookingMicroservice.Models
{
    public class FlightResponseWrapper
    {
        [JsonPropertyName("$values")]
        public List<Flight> Values { get; set; }
    }
}
