﻿namespace BookingMicroservice.Models
{
    public class BookingCreation
    {
        public required int FlightId { get; set; }
        public required BookingClass BookingClass { get; set; }
        public int? SeatId { get; set; }
        public required string PaymentIntentId { get; set; }
    }
}
