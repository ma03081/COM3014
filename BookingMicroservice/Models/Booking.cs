﻿namespace BookingMicroservice.Models
{
    public class Booking
    {
        public int Id { get; private set; }
        public int FlightId { get; private set; }
        public int UserId { get; private set; }
        public BookingClass BookingClass { get; private set; }
        public int? SeatId { get; private set; }

        public Booking(int flightId, int userId, BookingClass bookingClass, int? seatId)
        {
            FlightId = flightId;
            UserId = userId;
            BookingClass = bookingClass;
            SeatId = seatId;
        }

        public Booking() { }

        public void SetSeatNumber(int seatId)
        {
            SeatId = seatId;
        }
    }
}
