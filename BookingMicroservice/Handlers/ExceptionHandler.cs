﻿using System.Net;
using System.Text.Json;

namespace BookingMicroservice.Handlers
{
    public class ExceptionHandler
    {
        private readonly RequestDelegate next;

        public ExceptionHandler(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                if (!context.Response.HasStarted)
                {
                    var response = context.Response;
                    response.ContentType = "application/json";
                    response.StatusCode = (int)HttpStatusCode.InternalServerError;

                    var result = JsonSerializer.Serialize(new
                    {
                        response.StatusCode,
                        ex.Message
                    });

                    await response.WriteAsync(result);
                }
            }
        }

    }
}
