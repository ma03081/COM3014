﻿using System.Net;

namespace BookingMicroservice.Handlers
{
    public class RequestCookieHandler : DelegatingHandler
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public RequestCookieHandler(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpContext? context = httpContextAccessor.HttpContext;

            if (context?.Request.Cookies != null && request.RequestUri != null)
            {
                CookieContainer cookieContainer = new CookieContainer();
                foreach (KeyValuePair<string, string> cookie in context.Request.Cookies)
                {
                    if (!string.IsNullOrEmpty(cookie.Value) && (cookie.Key == "AccessToken" || cookie.Key == "RefreshToken"))
                        cookieContainer.Add(request.RequestUri, new Cookie(cookie.Key, cookie.Value));
                }

                var cookieHeader = cookieContainer.GetCookieHeader(request.RequestUri);
                if (!string.IsNullOrEmpty(cookieHeader))
                    request.Headers.Add("Cookie", cookieHeader);
            }

            return await base.SendAsync(request, cancellationToken);
        }


    }
}
