# Flight Booking Service Website

Welcome to the repository for the flight booking service website. This project utilizes a microservices architecture, with each microservice (Client App, Booking Microservice, User Microservice, Flight Microservice, and Gateway API) housed in its respective directory.

## CI/CD Pipeline
The `.gitlab-ci.yml` file contains the CI/CD pipeline configurations to automate the delivery of the website.

## Dockerization
The services are containerized using Docker. The `docker-compose.yml` file is used to build and run the services in Docker containers.

## Configuration
The `.env` file contains configuration settings that allow you to change the ports of the services, if there be any port conflicts.

## Building and Running Services
To build the services, run the following command in the terminal:
```bash
docker-compose build
```

After building, you can start the services using:

```bash
docker-compose up
```

## Addtional Information

The database image is pre-populated with default values for testing. Currently, the database is configured with an Airline account:

- Email: skyHigh@gmail.com
- Password: 123456789

This account manages two flights:

- From London Heathrow to Glasgow
- From London Heathrow to Liverpool

Both flights are scheduled for 15/7/2024.


For Booking flights with a regular user account, the credit card number used for testing this feature was `4242 4242 4242 4242`. Since the card is for testing, the expiration date and CVC can be arbitrary and will not affect the server's output.