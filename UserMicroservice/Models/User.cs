﻿using Microsoft.AspNetCore.Identity;

namespace UserMicroservice.Models
{
    public class User
    {
        // Parameterless constructor for EF Core
        public User() { }

        public User(string userName, string email, UserType userType)
        {
            Username = userName;
            Email = email;
            Type = userType;
        }

        public int Id { get; private set; }
        public string Username { get; private set; }
        public string Email { get; private set; }
        public string PasswordHash { get; private set; }
        public UserType Type { get; private set; } = UserType.CUSTOMER;

        public void SetPasswordHash(IPasswordHasher<User> passwordHasher, string password)
        {
            PasswordHash = passwordHasher.HashPassword(this, password);
        }

    }

    public enum UserType
    {
        CUSTOMER = 0,
        AIRLINE = 1
    }
}
