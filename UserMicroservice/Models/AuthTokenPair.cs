﻿namespace UserMicroservice.Models
{
    public class AuthTokenPair
    {
        public string AccessToken { get; }
        public string RefreshToken { get; }

        public AuthTokenPair(string accessToken, string refreshToken)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
        }
    }
}
