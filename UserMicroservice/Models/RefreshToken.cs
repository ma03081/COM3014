﻿namespace UserMicroservice.Models
{
    public class RefreshToken
    {
        public int Id { get; internal set; }
        public int UserId { get; internal set; }
        public string Token { get; internal set; }
        public DateTime ExpirationDate { get; internal set; }

        public RefreshToken() { }

        public RefreshToken(string token, DateTime expirationDate, int userId)
        {
            Token = token;
            ExpirationDate = expirationDate;
            UserId = userId;
        }
    }
}
