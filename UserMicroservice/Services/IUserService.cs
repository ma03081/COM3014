﻿using UserMicroservice.Models;

namespace UserMicroservice.Services
{
    // CRUD Based Service
    public interface IUserService
    {
        User? GetUser(string username);
        User? GetUser(int userId);
        User? GetUser(string email, string password);
        List<User> GetUsers();
        User CreateUser(string email, string userName, string password, UserType UserType);
        void UpdateUser(int id, string? username, string? email, string? password);
        bool DeleteUser(string username);
        User? GetUserByEmail(string email);
    }
}
