﻿using UserMicroservice.Models;

namespace UserMicroservice.Services
{
    public interface IAuthService
    {
        AuthTokenPair AuthenticateUser(int userId);
        void RevokeRefreshToken(string token);
        bool ValidateRefreshToken(string token);
        int? GetUserId(string refreshToken);
    }
}
