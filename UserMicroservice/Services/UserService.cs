﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UserMicroservice.Models;

namespace UserMicroservice.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;
        private readonly IPasswordHasher<User> _passwordHasher;

        public UserService(ApplicationDbContext context, IPasswordHasher<User> passwordHasher)
        {
            _context = context;
            _passwordHasher = passwordHasher;
        }

        public bool DeleteUser(string username)
        {
            User? user = _context.Users.SingleOrDefault(user => user.Username == username);
            if (user == null)
                return false;

            _context.Users.Remove(user);
            _context.SaveChanges();

            return true;
        }

        public List<User> GetUsers()
        {
            List<User> users = _context.Users.ToList();
            return users;
        }

        public User? GetUser(string username)
        {
            User? user = _context.Users.SingleOrDefault(user => user.Username == username);
            return user;
        }

        public User? GetUser(int userId)
        {
            User? user = _context.Users.SingleOrDefault(user => user.Id == userId);
            return user;
        }

        public User? GetUser(string email, string password)
        {
            User? user = GetUserByEmail(email);

            if (user != null)
            {
                // Verify the hashed password with the provided password
                var verificationResult = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
                if (verificationResult == PasswordVerificationResult.Success)
                    return user; // Password is correct, return the user
            }

            return null; // User not found or password does not match
        }

        public User? GetUserByEmail(string email)
        {
            User? user = _context.Users.SingleOrDefault(user => user.Email == email);
            return user;
        }

        public User CreateUser(string email, string userName, string password, UserType userType)
        {
            if (GetUser(userName) != null || GetUserByEmail(email) != null)
                throw new InvalidOperationException($"A User with the provided credntials already exists");

            User user = new User(userName, email, userType);
            user.SetPasswordHash(_passwordHasher, password);

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;            
        }

        public void UpdateUser(int id, string? username, string? email, string? password)
        {
            User? user = GetUser(id);
            if(user == null)
                throw new KeyNotFoundException($"A User with the provided Id {id} doesnt exist");
            
            string updatedEmail = string.IsNullOrEmpty(email) ? user.Email : email;
            string updatedUserName = string.IsNullOrEmpty(username) ? user.Username : username;
            string updatedPassword = user.PasswordHash;

            if (!string.IsNullOrEmpty(password)) {
                user.SetPasswordHash(_passwordHasher, password);
                updatedPassword = user.PasswordHash;
            }

            int affectedRows = _context.Users
                        .Where(user => user.Id == id)
                        .ExecuteUpdate(setters => setters
                            .SetProperty(user => user.Username, updatedUserName)
                            .SetProperty(user => user.Email, updatedEmail)
                            .SetProperty(user => user.PasswordHash, updatedPassword));

            _context.SaveChanges();

            if (affectedRows == 0)
                throw new DbUpdateException("Operation was not able to update the Database");
        }
    }
}
