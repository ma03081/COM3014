##################
# CI/CD Pipeline #
##################
stages:
  - build
  - test
  - deploy

.default_tags: &default_tags
  tags:
    - all-jobs

###############
# Build Stage #
###############
build_user_service:
  <<: *default_tags
  stage: build
  script:
    - echo "Building User Microservice"
    - dotnet publish UserMicroservice -c Release -o ./output/UserMicroservice
  artifacts:
    paths:
      - output/UserMicroservice/

build_flight_service:
  <<: *default_tags
  stage: build
  script:
    - echo "Building Flight Microservice"
    - dotnet publish FlightMicroservice -c Release -o ./output/FlightMicroservice
  artifacts:
    paths:
      - output/FlightMicroservice/

build_booking_service:
  <<: *default_tags
  stage: build
  script:
    - echo "Building Booking Microservice"
    - dotnet publish BookingMicroservice -c Release -o ./output/BookingMicroservice
  artifacts:
    paths:
      - output/BookingMicroservice/

build_gateway_api:
  <<: *default_tags
  stage: build
  script:
    - echo "Building Gateway API"
    - dotnet publish GatewayAPI -c Release -o ./output/GatewayAPI
  artifacts:
    paths:
      - output/GatewayAPI/

build_client_app:
  <<: *default_tags
  stage: build
  script:
    - echo "Building Client Application"
    - cd client
    - npm install
    - npm run build
    - mkdir -p ../output/client
    - cp -R build/* ../output/client/
  artifacts:
    paths:
      - output/client/

##############
# Test Stage #
##############
test-services:
  <<: *default_tags
  stage: test
  script:
    - echo "Running unit tests..."
    - find . -name "*.Tests.csproj" -exec dotnet test {} \;
  artifacts:
    when: always
    reports:
      junit: 
        - "**/TestResults/*.xml"
  coverage: '/^Total coverage: (\d+\.\d+)%/'

################
# Deploy Stage #
################
deploy_services_to_registry:
  <<: *default_tags
  stage: deploy
  script:
    - echo "Publishing User Microservice to GitLab Package Registry..."
    - dotnet nuget push "output/UserMicroservice/*.nupkg" --source "https://gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/packages/nuget" --api-key $CI_JOB_TOKEN
    - echo "Publishing Flight Microservice to GitLab Package Registry..."
    - dotnet nuget push "output/FlightMicroservice/*.nupkg" --source "https://gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/packages/nuget" --api-key $CI_JOB_TOKEN
    - echo "Publishing Booking Microservice to GitLab Package Registry..."
    - dotnet nuget push "output/BookingMicroservice/*.nupkg" --source "https://gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/packages/nuget" --api-key $CI_JOB_TOKEN
    - echo "Publishing Gateway API to GitLab Package Registry..."
    - dotnet nuget push "output/GatewayAPI/*.nupkg" --source "https://gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/packages/nuget" --api-key $CI_JOB_TOKEN
    - echo "Publishing Client Application to GitLab NPM Registry..."
    - cd output/client
    - npm pack
    - npm publish --registry "https://gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/packages/npm" --auth "//gitlab.example.com/api/v4/projects/$CI_PROJECT_ID/packages/npm/:_authToken=$CI_JOB_TOKEN"
  dependencies:
    - build_user_service
    - build_client_app
    - build_gateway_api
    - build_booking_service
    - build_flight_service
  only:
    - /^release\/.*$/

deploy_docker_images:
  <<: *default_tags
  stage: deploy
  script:
    - echo "Building and pushing Docker images..."
    - docker-compose -f docker-compose.yml build
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker-compose -f docker-compose.yml push
  only:
    - /^release\/.*$/
