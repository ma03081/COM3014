﻿namespace GatewayAPI.Models
{
    public class FlightCreation
    {
        public required string Origin { get; set; }
        public required string Destination { get; set; }
        public required DateTime DepartureTime { get; set; }
        public required DateTime ArrivalTime { get; set; }
        public required int EconomyCapacity { get; set; }
        public required int BusinessCapacity { get; set; }
        public required decimal EconomyPrice { get; set; }
        public required decimal BusinessPrice { get; set; }

    }
}
