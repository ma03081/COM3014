﻿namespace GatewayAPI.Models
{
    public class BookingCreation
    {
        public required int FlightId { get; set; }
        public required int BookingClass { get; set; }
        public int? SeatId { get; set; }
        public required string PaymentIntentId { get; set; }
    }
}
