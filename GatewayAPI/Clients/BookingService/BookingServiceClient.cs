﻿using GatewayAPI.Models;
using Microsoft.AspNetCore.WebUtilities;

namespace GatewayAPI.Clients.BookingService
{
    public class BookingServiceClient : IBookingServiceClient
    {
        private readonly HttpClient httpClient;
        private const string API_PATH = "api/Booking";

        public BookingServiceClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public Task<HttpResponseMessage> GetBookingAsync(int id)
        {
            return httpClient.GetAsync($"{API_PATH}/{id}");
        }

        public Task<HttpResponseMessage> GetBookingsAsync(int? flightId = null, int? userId = null, int? bookingClass = null)
        {
            Dictionary<string, string?> queryParams = new Dictionary<string, string?>();

            if (flightId.HasValue)
                queryParams.Add("flightId", flightId.Value.ToString());
            
            if (userId.HasValue)
                queryParams.Add("userId", userId.Value.ToString());

            if (bookingClass.HasValue)
                queryParams.Add("bookingClass", bookingClass.Value.ToString());

            string url = QueryHelpers.AddQueryString(API_PATH, queryParams);

            return httpClient.GetAsync(url);
        }

        public Task<HttpResponseMessage> MakeBookingAsync(BookingCreation bookingModel)
        {
            return httpClient.PostAsJsonAsync($"{API_PATH}", bookingModel);
        }

        public Task<HttpResponseMessage> UpdateBookingAsync(int bookindId, BookingUpdate bookingModel)
        {
            return httpClient.PutAsJsonAsync($"{API_PATH}/{bookindId}", bookingModel);
        }


        public Task<HttpResponseMessage> GetUpcomingFlightBookingsAsync()
        {
            return httpClient.GetAsync($"{API_PATH}/upcoming");
        }

        public Task<HttpResponseMessage> GetPreviousFlightBookingsAsync()
        {
            return httpClient.GetAsync($"{API_PATH}/history");
        }

        public Task<HttpResponseMessage> CreatePaymentIntent(PaymentIntentCreateRequest request)
        {
            return httpClient.PostAsJsonAsync($"{API_PATH}/create-payment-intent", request);
        }

    }
}
