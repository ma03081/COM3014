﻿using GatewayAPI.Models;

namespace GatewayAPI.Clients.BookingService
{
    public interface IBookingServiceClient
    {
        Task<HttpResponseMessage> GetBookingAsync(int id);
        Task<HttpResponseMessage> GetBookingsAsync(int? flightId = null, int? userId = null, int? bookingClass = null);
        Task<HttpResponseMessage> MakeBookingAsync(BookingCreation bookingModel);
        Task<HttpResponseMessage> UpdateBookingAsync(int bookindId, BookingUpdate bookingModel);
        Task<HttpResponseMessage> GetUpcomingFlightBookingsAsync();
        Task<HttpResponseMessage> GetPreviousFlightBookingsAsync();
        Task<HttpResponseMessage> CreatePaymentIntent(PaymentIntentCreateRequest request);
    }
}
