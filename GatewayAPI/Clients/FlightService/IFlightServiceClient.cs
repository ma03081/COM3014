﻿using GatewayAPI.Models;

namespace GatewayAPI.Clients.FlightService
{
    public interface IFlightServiceClient
    {
        Task<HttpResponseMessage> GetFlightAsync(int flightId);
        Task<HttpResponseMessage> GetFlightsAsync(int? airlineId = null, string? origin = null, string? destination = null, DateTime? departureTime = null, DateTime? arrivalTime = null);
        Task<HttpResponseMessage> AddFlightAsync(FlightCreation flight);
        Task<HttpResponseMessage> GetFlightCapacityAsync(int flightId, int classType);
        Task<HttpResponseMessage> GetFlightSeatsAsync(int flightId);


        Task<HttpResponseMessage> GetSeatsAsync();
        Task<HttpResponseMessage> GetSeatAsync(int seatId);
        Task<HttpResponseMessage> IsSeatAvailableAsync(int seatId);
        Task<HttpResponseMessage> BookSeatAsync(int seatId);
    }
}
