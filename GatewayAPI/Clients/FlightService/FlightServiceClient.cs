﻿using GatewayAPI.Models;

namespace GatewayAPI.Clients.FlightService
{
    public class FlightServiceClient : IFlightServiceClient
    {
        private readonly HttpClient httpClient;
        private static readonly string FLIGHT_API_PATH = "api/Flight";
        private static readonly string SEAT_API_PATH = "api/Seat";

        public FlightServiceClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> GetFlightAsync(int flightId)
        {
            return await httpClient.GetAsync($"{FLIGHT_API_PATH}/{flightId}");
        }

        public async Task<HttpResponseMessage> GetFlightsAsync(int? airlineId = null, string? origin = null, string? destination = null, DateTime? departureTime = null, DateTime? arrivalTime = null)
        {
            var queryParams = new List<string>();

            if(airlineId != null)
                queryParams.Add($"airlineId={airlineId}");

            if (!string.IsNullOrEmpty(origin))
                queryParams.Add($"origin={Uri.EscapeDataString(origin)}");

            if (!string.IsNullOrEmpty(destination))
                queryParams.Add($"destination={Uri.EscapeDataString(destination)}");

            if (departureTime.HasValue)
                queryParams.Add($"departureTime={departureTime.Value.ToString("yyyy-MM-dd HH:mm:ss.fff") + "000"}");

            if (arrivalTime.HasValue)
                queryParams.Add($"arrivalTime={arrivalTime.Value.ToString("yyyy-MM-dd HH:mm:ss.fff") + "000"}");

            string queryString = queryParams.Any() ? $"?{string.Join("&", queryParams)}" : string.Empty;

            return await httpClient.GetAsync($"{FLIGHT_API_PATH}{queryString}");
        }

        public async Task<HttpResponseMessage> AddFlightAsync(FlightCreation flight)
        {
            return await httpClient.PostAsJsonAsync(FLIGHT_API_PATH, flight);
        }

        public async Task<HttpResponseMessage> GetFlightCapacityAsync(int flightId, int classType)
        {
            return await httpClient.GetAsync($"{FLIGHT_API_PATH}/{flightId}/capacity?ClassType={classType}");
        }

        public async Task<HttpResponseMessage> GetFlightSeatsAsync(int flightId)
        {
            return await httpClient.GetAsync($"{FLIGHT_API_PATH}/{flightId}/seats");
        }





        public async Task<HttpResponseMessage> GetSeatsAsync()
        {
            return await httpClient.GetAsync(SEAT_API_PATH);
        }

        public async Task<HttpResponseMessage> GetSeatAsync(int seatId)
        {
            return await httpClient.GetAsync($"{SEAT_API_PATH}/{seatId}");
        }

        public async Task<HttpResponseMessage> IsSeatAvailableAsync(int seatId)
        {
            return await httpClient.GetAsync($"{SEAT_API_PATH}/{seatId}/isAvailable");
        }

        public async Task<HttpResponseMessage> BookSeatAsync(int seatId)
        {
            return await httpClient.PutAsync($"{SEAT_API_PATH}/{seatId}", null);
        }
    }
}
