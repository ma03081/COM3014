﻿using GatewayAPI.Models;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;

namespace GatewayAPI.Clients.UserService
{
    public class UserServiceClient : IUserServiceClient
    {
        private readonly HttpClient httpClient;
        private static readonly string API_PATH = "api/User";

        public UserServiceClient(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> GetUserAsync(int id)
        {
            return await httpClient.GetAsync($"{API_PATH}/{id}");
        }

        public async Task<HttpResponseMessage> GetUsersAsync()
        {
            return await httpClient.GetAsync($"{API_PATH}");
        }

        public async Task<HttpResponseMessage> RegisterUserAsync(UserRegistration user)
        {
            return await httpClient.PostAsJsonAsync($"{API_PATH}/register", user);
        }

        public async Task<HttpResponseMessage> LoginUserAsync(UserLogin user)
        {
            return await httpClient.PostAsJsonAsync($"{API_PATH}/login", user);
        }

        public async Task<HttpResponseMessage> AuthorizeUserAsync()
        {
            return await httpClient.PostAsync($"{API_PATH}/authorize", null);
        }

        public async Task<HttpResponseMessage> LogoutUserAsync()
        {
            return await httpClient.PostAsync($"{API_PATH}/logout", null);
        }

        public async Task<HttpResponseMessage> UpdateUserAsync(int id, UserUpdateInfo updateInfo)
        {
            return await httpClient.PatchAsJsonAsync($"{API_PATH}/{id}", updateInfo);
        }

    }
}
