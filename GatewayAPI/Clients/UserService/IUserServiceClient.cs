﻿using GatewayAPI.Models;

namespace GatewayAPI.Clients.UserService
{
    public interface IUserServiceClient
    {
        Task<HttpResponseMessage> GetUserAsync(int id);
        Task<HttpResponseMessage> GetUsersAsync();
        Task<HttpResponseMessage> RegisterUserAsync(UserRegistration user);
        Task<HttpResponseMessage> LoginUserAsync(UserLogin user);
        Task<HttpResponseMessage> AuthorizeUserAsync();
        Task<HttpResponseMessage> LogoutUserAsync();
        Task<HttpResponseMessage> UpdateUserAsync(int id, UserUpdateInfo updateInfo);
    }
}
