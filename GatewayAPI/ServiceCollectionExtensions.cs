﻿using GatewayAPI.Clients.BookingService;
using GatewayAPI.Clients.FlightService;
using GatewayAPI.Clients.UserService;
using System.Runtime.CompilerServices;

namespace GatewayAPI
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddHttpClients(this IServiceCollection services, ConfigurationManager configurationManager)
        {
            services.AddHttpClient<IUserServiceClient, UserServiceClient>(client =>
            {
                string baseUrl = configurationManager["UserMicroservice:BaseUrl"] ?? throw new InvalidOperationException("UserMicroservice BaseUrl is not configured.");
                client.BaseAddress = new Uri(baseUrl.EndsWith("/") ? baseUrl : baseUrl + "/");
            }).AddHttpMessageHandler<RequestCookieHandler>();

            services.AddHttpClient<IFlightServiceClient, FlightServiceClient>(client =>
            {
                string baseUrl = configurationManager["FlightMicroservice:BaseUrl"] ?? throw new InvalidOperationException("FlightMicroservice BaseUrl is not configured.");
                client.BaseAddress = new Uri(baseUrl.EndsWith("/") ? baseUrl : baseUrl + "/");
            }).AddHttpMessageHandler<RequestCookieHandler>();

            services.AddHttpClient<IBookingServiceClient, BookingServiceClient>(client =>
            {
                string baseUrl = configurationManager["BookingMicroservice:BaseUrl"] ?? throw new InvalidOperationException("BookingMicroservice BaseUrl is not configured.");
                client.BaseAddress = new Uri(baseUrl.EndsWith("/") ? baseUrl : baseUrl + "/");
            }).AddHttpMessageHandler<RequestCookieHandler>();

            return services;
        }

        public static IServiceCollection AddCorsPolicy(this IServiceCollection services, ConfigurationManager configurationManager) 
        {
            string baseUrl = configurationManager["ClientService:BaseUrl"] ?? throw new InvalidOperationException("ClientService BaseUrl is not configured.");
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAirlineClient", builder =>
                {
                    builder.WithOrigins(baseUrl)
                           .AllowAnyMethod()
                           .AllowAnyHeader()
                           .AllowCredentials();
                });
            });

            return services;
        }
    }
}
