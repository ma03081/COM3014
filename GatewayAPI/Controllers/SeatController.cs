﻿using GatewayAPI.Clients.FlightService;
using Microsoft.AspNetCore.Mvc;

namespace GatewayAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SeatController : ControllerBase
    {
        private readonly IFlightServiceClient flightServiceClient;
        public SeatController(IFlightServiceClient flightServiceClient)
        {
            this.flightServiceClient = flightServiceClient;
        }

        [HttpGet()]
        public async Task<IActionResult> GetSeats()
        {
            HttpResponseMessage response = await flightServiceClient.GetSeatsAsync();
            return new HttpResponseMessageResult(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSeats(int id)
        {
            HttpResponseMessage response = await flightServiceClient.GetSeatAsync(id);
            return new HttpResponseMessageResult(response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> BookSeat(int id)
        {
            HttpResponseMessage response = await flightServiceClient.BookSeatAsync(id);
            return new HttpResponseMessageResult(response);
        }

        [HttpGet("{id}/isAvailable")]
        public async Task<IActionResult> IsAvailable(int id)
        {
            HttpResponseMessage response = await flightServiceClient.IsSeatAvailableAsync(id);
            return new HttpResponseMessageResult(response);
        }

    }
}
