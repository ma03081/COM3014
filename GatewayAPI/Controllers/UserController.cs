﻿using GatewayAPI.Clients.UserService;
using GatewayAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace GatewayAPI.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserServiceClient userServiceClient;
        public UserController(IUserServiceClient userServiceClient)
        {
            this.userServiceClient = userServiceClient;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserRegistration userRegistration)
        {
            HttpResponseMessage response = await userServiceClient.RegisterUserAsync(userRegistration);
            return new HttpResponseMessageResult(response);
        }

        [HttpPost("authorize")]
        public async Task<IActionResult> Authorize()
        {
            HttpResponseMessage response = await userServiceClient.AuthorizeUserAsync();
            return new HttpResponseMessageResult(response);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserLogin userLogin)
        {
            HttpResponseMessage response = await userServiceClient.LoginUserAsync(userLogin);
            return new HttpResponseMessageResult(response);
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            HttpResponseMessage response = await userServiceClient.LogoutUserAsync();
            return new HttpResponseMessageResult(response);
        }

        [HttpGet()]
        public async Task<IActionResult> GetUsers()
        {
            HttpResponseMessage response = await userServiceClient.GetUsersAsync();
            return new HttpResponseMessageResult(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            HttpResponseMessage response = await userServiceClient.GetUserAsync(id);
            return new HttpResponseMessageResult(response);
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody] UserUpdateInfo updateInfo)
        {
            HttpResponseMessage response = await userServiceClient.UpdateUserAsync(id, updateInfo);
            return new HttpResponseMessageResult(response);
        }
    }

}
