﻿using GatewayAPI.Clients.FlightService;
using GatewayAPI.Clients.UserService;
using GatewayAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace GatewayAPI.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class FlightController : ControllerBase
    {
        private readonly IFlightServiceClient flightServiceClient;
        public FlightController(IFlightServiceClient flightServiceClient)
        {
            this.flightServiceClient = flightServiceClient;
        }

        [HttpGet()]
        public async Task<IActionResult> GetFlights(int? airlineId = null, string? origin = null, string? destination = null, DateTime? departureTime = null, DateTime? arrivalTime = null)
        {
            HttpResponseMessage response = await flightServiceClient.GetFlightsAsync(airlineId, origin, destination, departureTime, arrivalTime);
            return new HttpResponseMessageResult(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFlights(int id)
        {
            HttpResponseMessage response = await flightServiceClient.GetFlightAsync(id);
            return new HttpResponseMessageResult(response);
        }

        [HttpPost()]
        public async Task<IActionResult> AddFlight([FromBody] FlightCreation flightCreation)
        {
            HttpResponseMessage response = await flightServiceClient.AddFlightAsync(flightCreation);
            return new HttpResponseMessageResult(response);
        }


        [HttpGet("{id}/capacity")]
        public async Task<IActionResult> GetFlightCapacity([FromRoute] int id, [FromQuery] int classType)
        {
            HttpResponseMessage response = await flightServiceClient.GetFlightCapacityAsync(id, classType);
            return new HttpResponseMessageResult(response);
        }

        [HttpGet("{id}/seats")]
        public async Task<IActionResult> GetFlightSeats([FromRoute] int id)
        {
            HttpResponseMessage response = await flightServiceClient.GetFlightSeatsAsync(id);
            return new HttpResponseMessageResult(response);
        }

    }
}
