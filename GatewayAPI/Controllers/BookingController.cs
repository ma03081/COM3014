﻿using GatewayAPI.Clients.BookingService;
using GatewayAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace GatewayAPI.Controllers
{

    [ApiController]
    [Route("api/[Controller]")]
    public class BookingController : ControllerBase
    {
        private readonly IBookingServiceClient bookingServiceClient;

        public BookingController(IBookingServiceClient bookingServiceClient)
        {
            this.bookingServiceClient = bookingServiceClient;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBooking(int id) 
        {
            HttpResponseMessage response = await bookingServiceClient.GetBookingAsync(id);
            return new HttpResponseMessageResult(response);
        }

        [HttpGet()]
        public async Task<IActionResult> GetBookings(int? flightId = null, int? userId = null, int? bookingClass = null)
        {
            HttpResponseMessage response = await bookingServiceClient.GetBookingsAsync(flightId, userId, bookingClass);
            return new HttpResponseMessageResult(response);
        }

        [HttpPost()]
        public async Task<IActionResult> MakeBooking([FromBody] BookingCreation bookingCreationModel)
        {
            HttpResponseMessage response = await bookingServiceClient.MakeBookingAsync(bookingCreationModel);
            return new HttpResponseMessageResult(response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBooking([FromRoute] int id, [FromBody] BookingUpdate bookingUpdateModel)
        {
            HttpResponseMessage response = await bookingServiceClient.UpdateBookingAsync(id, bookingUpdateModel);
            return new HttpResponseMessageResult(response);
        }

        [HttpGet("upcoming")]
        public async Task<IActionResult> GetUpcomingFlightBookings()
        {
            HttpResponseMessage response = await bookingServiceClient.GetUpcomingFlightBookingsAsync();
            return new HttpResponseMessageResult(response);
        }

        [HttpGet("history")]
        public async Task<IActionResult> GetPreviousFlightBookings()
        {
            HttpResponseMessage response = await bookingServiceClient.GetPreviousFlightBookingsAsync();
            return new HttpResponseMessageResult(response);
        }

        [HttpPost("create-payment-intent")]
        public async  Task<IActionResult> CreatePaymentIntent([FromBody] PaymentIntentCreateRequest request)
        {
            HttpResponseMessage response = await bookingServiceClient.CreatePaymentIntent(request);
            return new HttpResponseMessageResult(response);
        }
    }
}
