CREATE TABLE Users (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Username LONGTEXT NOT NULL,
    Email LONGTEXT NOT NULL,
    PasswordHash LONGTEXT NOT NULL,
    Type INT NOT NULL
);

CREATE TABLE RefreshTokens (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    UserId INT NOT NULL,
    Token LONGTEXT NOT NULL,
    ExpirationDate DATETIME(6) NOT NULL
);

CREATE TABLE Flights (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    AirlineId INT NOT NULL,
    Origin LONGTEXT NOT NULL,
    Destination LONGTEXT NOT NULL,
    DepartureTime DATETIME(6) NOT NULL,
    ArrivalTime DATETIME(6) NOT NULL,
    EconomyCapacity INT NOT NULL,
    BusinessCapacity INT NOT NULL,
    EconomyPrice DECIMAL(19, 2) NOT NULL,
    BusinessPrice DECIMAL(19, 2) NOT NULL
);

CREATE TABLE Seats (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    FlightId INT NOT NULL,
    SeatNumber LONGTEXT NOT NULL,
    ClassType INT NOT NULL,
    IsAvailable TINYINT(1) NOT NULL
);

CREATE TABLE Bookings (
    Id INT(11) NOT NULL AUTO_INCREMENT,
    FlightId INT(11) NOT NULL,
    UserId INT(11) NOT NULL,
    BookingClass INT(11) NOT NULL,
    SeatId INT(11) DEFAULT NULL,
    PRIMARY KEY (Id)
);

