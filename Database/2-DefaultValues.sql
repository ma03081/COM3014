INSERT INTO Users (Username, Email, PasswordHash, Type) VALUES 
('SkyHigh Airlines', 'skyHigh@gmail.com', 'AQAAAAIAAYagAAAAEL9Xj4ZjCgTc/w0XbnjHE63KPvhYmJdTZV9k570Bw0QuPsrupU8EGS7kT7hUoGZdbg==', 1);

INSERT INTO Flights (AirlineId, Origin, Destination, DepartureTime, ArrivalTime, EconomyCapacity, BusinessCapacity, EconomyPrice, BusinessPrice) VALUES 
(1, 'London Heathrow', 'Liverpool', '2024-07-15 08:00:00', '2024-07-15 12:00:00', 24, 8, 200.00, 500.00),
(1, 'London Heathrow', 'Glasgow', '2024-07-15 08:00:00', '2024-07-15 12:00:00', 24, 8, 150.00, 350.00);


INSERT INTO Seats (FlightId, SeatNumber, ClassType, IsAvailable) VALUES 
(1, '1A', 0, 1),
(1, '1B', 0, 1),
(1, '1C', 0, 1),
(1, '1D', 0, 1),
(1, '2A', 0, 1),
(1, '2B', 0, 1),
(1, '2C', 0, 1),
(1, '2D', 0, 1),
(2, '1A', 0, 1),
(2, '1B', 0, 1),
(2, '1C', 0, 1),
(2, '1D', 0, 1),
(2, '2A', 0, 1),
(2, '2B', 0, 1),
(2, '2C', 0, 1),
(2, '2D', 0, 1);

INSERT INTO Seats (FlightId, SeatNumber, ClassType, IsAvailable) VALUES 
(1, '3A', 1, 1),
(1, '3B', 1, 1),
(1, '3C', 1, 1),
(1, '3D', 1, 1),
(1, '3E', 1, 1),
(1, '3F', 1, 1),
(1, '4A', 1, 1),
(1, '4B', 1, 1),
(1, '4C', 1, 1),
(1, '4D', 1, 1),
(1, '4E', 1, 1),
(1, '4F', 1, 1),
(1, '5A', 1, 1),
(1, '5B', 1, 1),
(1, '5C', 1, 1),
(1, '5D', 1, 1),
(1, '5E', 1, 1),
(1, '5F', 1, 1),
(1, '6A', 1, 1),
(1, '6B', 1, 1),
(1, '6C', 1, 1),
(1, '6D', 1, 1),
(1, '6E', 1, 1),
(1, '6F', 1, 1),
(2, '3A', 1, 1),
(2, '3B', 1, 1),
(2, '3C', 1, 1),
(2, '3D', 1, 1),
(2, '3E', 1, 1),
(2, '3F', 1, 1),
(2, '4A', 1, 1),
(2, '4B', 1, 1),
(2, '4C', 1, 1),
(2, '4D', 1, 1),
(2, '4E', 1, 1),
(2, '4F', 1, 1),
(2, '5A', 1, 1),
(2, '5B', 1, 1),
(2, '5C', 1, 1),
(2, '5D', 1, 1),
(2, '5E', 1, 1),
(2, '5F', 1, 1),
(2, '6A', 1, 1),
(2, '6B', 1, 1),
(2, '6C', 1, 1),
(2, '6D', 1, 1),
(2, '6E', 1, 1),
(2, '6F', 1, 1);



